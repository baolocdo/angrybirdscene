#include "Plane.h"


Plane::Plane(float size, mat4 transformMatrix, vec4 ambient, vec4 diffuse, vec4 specular, vec4 emission, float shininess)
	: Entity(size, transformMatrix, ambient, diffuse, specular, emission, shininess)
{
}


Plane::~Plane(void)
{
}

void Plane::render(mat4 modelViewMatrix) {
	Entity::render(modelViewMatrix);
	glRectf(-1.0f,1.0f, 1.0f, -1.0f);
}


	