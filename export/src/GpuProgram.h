#pragma once

#include "stdafx.h"
#include "Light.h"

class GpuProgram
{
public:
	GpuProgram(string vertexShaderFile, string fragmentShaderFile);
	~GpuProgram(void);

	void setIsLight(bool isLight);
	void setIsShaded(bool isShaded);
	void setIsTex(bool isTex);
	void setIsDisplacementMap(bool isDisplacementMap);

	void setLights(vector<Light*> lights, mat4 modelViewMatrix);
	void setLightNum(int lightNum);
	void setTex(int tex);
	void setDisplacementMap(int map);

	void setAmbient(vec4 ambient);
	void setDiffuse(vec4 diffuse);
	void setSpecular(vec4 specular);
	void setEmission(vec4 emission);
	void setShininess(float shininess);

private:
	GLuint initShader(GLenum type, string file);
	GLuint initProgram(GLuint vertexShader, GLuint fragmentShader);

	string readShader(string filename);
	void programError(const GLint program);
	void shaderError(const GLint shader);

	GLuint vertexshader;
	GLuint fragmentshader;
	GLuint shaderprogram;

	// shaders variables
	GLuint mIsLight;
	GLuint mIsShaded;
	GLuint mIsTex;
	GLuint mIsDisplacementMap;

	GLuint* mLightPositions;
	GLuint* mLightColor;
	GLuint mLightNum;

	GLuint mTex;
	GLuint mDisplacementMap;

	GLuint mAmbient; 
	GLuint mDiffuse; 
	GLuint mSpecular; 
	GLuint mEmission; 
	GLuint mShininess;
};

