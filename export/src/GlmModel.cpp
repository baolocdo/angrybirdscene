#include "GlmModel.h"
#include "SceneManager.h"

GlmModel::GlmModel(void)
{
	mIsDisplacementMap = false;
	mDisplayWireframe = false;
}


GlmModel::~GlmModel(void)
{
	delete mMaterial;
	delete mGLMModel;
}

void GlmModel::scaleToUnit() {
	glmUnitize(mGLMModel);
}

void GlmModel::render() {
	SceneManager::getInstance()->getGpuProgram()->setAmbient(mMaterial->mAmbient);
	SceneManager::getInstance()->getGpuProgram()->setDiffuse(mMaterial->mDiffuse);
	SceneManager::getInstance()->getGpuProgram()->setSpecular(mMaterial->mSpecular);
	SceneManager::getInstance()->getGpuProgram()->setEmission(mMaterial->mEmission);
	SceneManager::getInstance()->getGpuProgram()->setShininess(mMaterial->mShininess);

	if (mIsTextured) {
		SceneManager::getInstance()->getGpuProgram()->setIsTex(true);
		SceneManager::getInstance()->getGpuProgram()->setTex(GL_TEXTURE0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, mTextureID);
	}  else {
		SceneManager::getInstance()->getGpuProgram()->setIsTex(false);
	}

	if (mIsDisplacementMap) {
		SceneManager::getInstance()->getGpuProgram()->setIsDisplacementMap(true);
		SceneManager::getInstance()->getGpuProgram()->setDisplacementMap(GL_TEXTURE1);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, mDisplacementMapID);
	}  else {
		SceneManager::getInstance()->getGpuProgram()->setIsDisplacementMap(false);
	}

	if (mDisplayWireframe)
		glmDraw(mGLMModel, GLM_SMOOTH | GLM_MATERIAL, mDisplayWireframe);
	else
		glmDraw(mGLMModel, GLM_SMOOTH | GLM_TEXTURE | GLM_MATERIAL);
}

