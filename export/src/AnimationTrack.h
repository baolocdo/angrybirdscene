#pragma once

#include "AnimationFrame.h"
#include "Entity.h"

class AnimationTrack
{
public:
	AnimationTrack(float amount);
	~AnimationTrack(void);

	void attachEntity(Entity* entity);
	void update(float timeElapsed);
	void reset();

	vector<AnimationFrame*> mAnimationFrames;
	vector<AnimationFrame*>::iterator mCurrentFrameIterator;
	vector<AnimationFrame*>::iterator mNextFrameIterator;
	Entity* mEntity;
	float mTime;

	float mAmount;
};

