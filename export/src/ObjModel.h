#pragma once
#include "model.h"
class ObjModel :
	public Model
{
public:
	ObjModel(void);
	~ObjModel(void);

	virtual void scaleToUnit();
	virtual void render();

	string mPathName;
	string mMaterialLibName;

	vector<vec3> mVertices;
	vector<vec3> mNormals;
	vector<vec3> mTexCoords;
	vector<vec3> mFaceNormals;
	vector<int> mIndices;
	vector<Triangle*> mTriangles;
	vector<Material*> mMaterials;
	map<string, Mesh*> mMeshes;
	vector<Texture*> mTextures;

	vec3 mPosition;
};

