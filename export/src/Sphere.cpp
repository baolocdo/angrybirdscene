#include "Sphere.h"

Sphere::Sphere(float size, mat4 transformMatrix, vec4 ambient, vec4 diffuse, vec4 specular, vec4 emission, float shininess)
	: Entity(size, transformMatrix, ambient, diffuse, specular, emission, shininess)
{
}


Sphere::~Sphere(void)
{
}

void Sphere::render(mat4 modelViewMatrix) {
	Entity::render(modelViewMatrix);
	glutSolidSphere(mSize, 20, 20);
}