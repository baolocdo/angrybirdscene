﻿#include "stdafx.h"

#include "Camera.h"
#include "Quaternion.h"

#include <iostream>

using namespace std;

Camera::Camera() {
}

Camera::Camera(vec3 position, vec3 up, vec3 lookAt, float fov)
	: mPosition(position)
	, mLookAt(lookAt)
	, mFov(fov)
	, mZMin(2.0)
	, mZMax(500.0)
	, mInitialPosition(position)
	, mInitialLookAt(lookAt)
{
	mLeft = glm::normalize(glm::cross(mPosition - mLookAt, up));
	mUp = glm::normalize(glm::cross(mLookAt - mPosition, mLeft));
	mInitialUp = up;
}

Camera::Camera(vec3 position, vec3 up, vec3 lookAt, float fov, float zMin, float zMax)
	: mPosition(position)
	, mLookAt(lookAt)
	, mFov(fov)
	, mZMin(zMin)
	, mZMax(zMax)
	, mInitialPosition(position)
	, mInitialLookAt(lookAt)
{
	mLeft = glm::normalize(glm::cross(mPosition - mLookAt, up));
	mUp = glm::normalize(glm::cross(mLookAt - mPosition, mLeft));
	mInitialUp = up;
}

Camera::~Camera(void)
{
}

void Camera::yaw(float degree) {
	if (degree < 1) {
		mLookAt = Quaternion(-degree, mUp) * (mLookAt - mPosition) + mPosition;
		mLeft = glm::normalize(glm::cross(mPosition - mLookAt, mUp));
	}
}
void Camera::pitch(float degree) {
	if (degree < 1) {
		mLookAt = Quaternion(degree, mLeft) * (mLookAt - mPosition) + mPosition;
		mUp = glm::normalize(glm::cross(mLookAt - mPosition, mLeft));
	}
}
void Camera::roll(float degree) {
	if (degree < 1) {
		mLookAt = Quaternion(degree, glm::normalize(mLookAt - mPosition)) * mUp;
	}
}

void Camera::moveForward(float amount){
	vec3 displacement = glm::normalize(mLookAt - mPosition) * amount;
	mPosition += displacement;
	mLookAt += displacement;
}

void Camera::moveBackward(float amount){
	vec3 displacement = glm::normalize(mLookAt - mPosition) * amount;
	mPosition -= displacement;
	mLookAt -= displacement;
}

void Camera::moveLeft(float amount){
	vec3 displacement = mLeft * amount;
	mPosition += displacement;
	mLookAt += displacement;
}

void Camera::moveRight(float amount){
	vec3 displacement = mLeft * amount;
	mPosition -= displacement;
	mLookAt -= displacement;
}

mat4 Camera::getModelViewMatrix() {
	return glm::lookAt(mPosition, mLookAt, mUp);
}