#pragma once

#include "stdafx.h"
#include "Texture.h"

class Material
{
public:
	Material(string name);
	Material(string name, vec4 ambient, vec4 diffuse, vec4 specular, vec4 emission, float shininess, Texture* texture);

	~Material();
	//Material(vec4 ambient, vec4 diffuse, vec4 specular, vec4 emission, float shininess);
	//~Material(void);

	static Material* DEFAULT;

	//vec4 getAmbient() const {return mAmbient;}
	//vec4 getDiffuse() const {return mDiffuse;}
	//vec4 getSpecular() const {return mSpecular;}
	//vec4 getEmission() const {return mEmission;}
	//float getShininess() const {return mShininess;}
	//GLuint getTextureID() const {return mTextureID;}

	//void setAmbient(const vec4 ambient) {mAmbient = ambient;}
	//void setDiffuse(const vec4 diffuse) {mDiffuse = diffuse;}
	//void setSpecular(const vec4 specular) {mSpecular = specular;}
	//void setEmission(const vec4 emission) {mEmission = emission;}
	//void setShininess(float shininess) {mShininess = shininess;}
	//void setTexture(TGAImg* texture) {mTexture = texture;}

	//string getName();
//private:
	string mName;
	vec4 mAmbient;
	vec4 mDiffuse;
	vec4 mSpecular;
	vec4 mEmission;
	float mShininess;
	Texture* mTexture;
};