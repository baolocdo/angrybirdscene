#include "Mesh.h"
#include "Triangle.h"

#define BUFFER_OFFSET(bytes) ((GLubyte *) NULL + (bytes))

Mesh::Mesh(string name)
	: mName(name)
{
}

Mesh::~Mesh(void)
{
}

void Mesh::generateVertexData(vector<vec3> vertices, vector<vec3> normals, vector<vec3> texCoords, vector<int> indices) {
	glGenBuffers(4, mBuffers);

	for (int i = 0; i < mTriangles.size(); i++) {
		mIndices.push_back(i * 3 + 0);
		mIndices.push_back(i * 3 + 1);
		mIndices.push_back(i * 3 + 2);

		mVerticesData.push_back(vertices[mTriangles[i]->getVIndices()[0]]);
		mNormalsData.push_back(normals[mTriangles[i]->getNIndices()[0]]);
		mTexCoordsData.push_back(texCoords[mTriangles[i]->getTIndices()[0]]);

		mVerticesData.push_back(vertices[mTriangles[i]->getVIndices()[1]]);
		mNormalsData.push_back(normals[mTriangles[i]->getNIndices()[1]]);
		mTexCoordsData.push_back(texCoords[mTriangles[i]->getTIndices()[1]]);

		mVerticesData.push_back(vertices[mTriangles[i]->getVIndices()[2]]);
		mNormalsData.push_back(normals[mTriangles[i]->getNIndices()[2]]);
		mTexCoordsData.push_back(texCoords[mTriangles[i]->getTIndices()[2]]);
	}

	glBindBuffer(GL_ARRAY_BUFFER, mBuffers[0]) ; 
	glBufferData(GL_ARRAY_BUFFER, 3 * mVerticesData.size(), &mVerticesData, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, mBuffers[1]) ; 
	glBufferData(GL_ARRAY_BUFFER, 3 * mNormalsData.size(), &mNormalsData, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, mBuffers[2]) ; 
	glBufferData(GL_ARRAY_BUFFER, 2 * mTexCoordsData.size(), &mTexCoordsData, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mBuffers[3]) ; 
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, mIndices.size(), &mIndices, GL_STATIC_DRAW);
}

void Mesh::render() {
 //   glEnableClientState(GL_VERTEX_ARRAY);
 //   glDisableClientState(GL_NORMAL_ARRAY);
	//glDisableClientState(GL_COLOR_ARRAY);
 //   glDisableClientState(GL_TEXTURE_COORD_ARRAY);

	//glBindBuffer(GL_ARRAY_BUFFER, mBuffers[0]);
	//glVertexPointer(3, GL_FLOAT, 0, BUFFER_OFFSET(0));

	//glBindBuffer(GL_ARRAY_BUFFER, mBuffers[1]);
	//glNormalPointer(GL_FLOAT, 0, BUFFER_OFFSET(0));

	//glBindTexture(GL_TEXTURE_2D, mMaterial->mTexture->getID()) ; 
	//glBindBuffer(GL_ARRAY_BUFFER, mBuffers[2]);
	//glTexCoordPointer(2, GL_FLOAT, 0, BUFFER_OFFSET(0));

	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mBuffers[3]);
	////glDrawElements(GL_TRIANGLES, mIndices.size(), GL_UNSIGNED_INT, BUFFER_OFFSET(0));
	//glDrawArrays(GL_POLYGON, 0, mIndices.size());

}

