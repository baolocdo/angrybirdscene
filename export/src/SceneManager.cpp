#include "SceneManager.h"
#include "Transform.h"
#include "GpuProgram.h"
#include "Camera.h"
#include "Scene.h"

#include <iostream>
#include <Windows.h>

SceneManager* SceneManager::mInstance = NULL;

// static handlers for GLUT callbacks.
static void onRender() {
	SceneManager::getInstance()->handleRender();
}

static void onIdle() {
	SceneManager::getInstance()->handleIdle();
}

static void onSpecialKeyDown(int key, int x, int y) {
	SceneManager::getInstance()->getInputManager()->onKeyDown(key, x, y);
}

static void onSpecialKeyUp(int key, int x, int y) {
	SceneManager::getInstance()->getInputManager()->onKeyUp(key, x, y);
}

static void onKeyDown(unsigned char key, int x, int y) {
	SceneManager::getInstance()->getInputManager()->onKeyDown(key, x, y);
}

static void onKeyUp(unsigned char key, int x, int y) {
	SceneManager::getInstance()->getInputManager()->onKeyUp(key, x, y);
}

static void onReshape(int width, int height) {
	SceneManager::getInstance()->handleReshape(width, height);
}

static void onPassiveMouse(int x, int y){
	SceneManager::getInstance()->handlePassiveMouse(x, y);
}

static void onMouse(int button, int state, int x, int y) {
	SceneManager::getInstance()->handleMouse(button, state, x, y);
}

static void onActiveMouse(int x, int y){
	SceneManager::getInstance()->handleMouseDrag(x, y);
}

static void onMouseEntry(int state){
	SceneManager::getInstance()->handleMouseEntry(state);
}

SceneManager::SceneManager(void)
{
	mMoveSpeed = 5;
	mAmount = 2;

	mSx = 1.0;
	mSy = 1.0;
	mTx = 0.0;
	mTy = 0.0 ;

	//Set light and tex on
	shaderOn = true;
	lightOn = true;
	texOn = false;
	disOn = false;

	mIsRotate = false;
	//Set the value for zooming
	mZoomFactor = 1.0;
	mMaxZoom = 2.0f;
	mMinZoom = 0.0f;

}

SceneManager::~SceneManager(void)
{
	if (mProgram) {
		delete mProgram;
	}
	if (mScene) {
		delete mScene;
	}
}

SceneManager* SceneManager::getInstance() {
	if (mInstance == NULL) {
		mInstance = new SceneManager();
	}
	return mInstance;
}

void SceneManager::initWindow(string title) {
	initWindow(title, INIT_WINDOW_POSITION_X, INIT_WINDOW_POSITION_Y);
}

void SceneManager::initWindow(string title, int posX, int posY) {
	mInputManager = new InputManager();

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowPosition(posX, posY);
	glutCreateWindow(title.c_str());

	//Set the mouse to the center
	glutWarpPointer(mCenterX, mCenterY);

	glutDisplayFunc(onRender);
	glutIdleFunc(onIdle);
	glutReshapeFunc(onReshape);
	
	glutSpecialFunc(onSpecialKeyDown);
	glutSpecialUpFunc(onSpecialKeyUp);
	glutKeyboardFunc(onKeyDown);
	glutKeyboardUpFunc(onKeyUp);
	glutPassiveMotionFunc(onPassiveMouse);

	glewInit();
}

void SceneManager::load(string file, string vertexShader, string fragmentShader) {
	mScene = new Scene();
	mScene->loadScene(file);
	glutInitWindowSize(mScene->getWidth(), mScene->getHeight());
	glutReshapeWindow(mScene->getWidth(), mScene->getHeight());
	//Set the center point
	mCenterX = mScene->getWidth() / 2;
	mCenterY = mScene->getHeight() / 2;

	mProgram = new GpuProgram(vertexShader, fragmentShader);
	// GL initialization
	glClearColor (0.0, 0.0, 0.0, 0.0);

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST) ;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW) ;
	glLoadIdentity() ;

	// TODO: google what is this
	glShadeModel(GL_SMOOTH);
}

void SceneManager::run() {
	mIsRunning = true;
	glutMainLoop();
}
void SceneManager::pause() {
	// TODO: implement
}

void SceneManager::handleRender() {
	handleKeyboard();

	glClearColor(0, 0, 1, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	mat4 modelViewMatrix;
	modelViewMatrix = mScene->getCamera()->getModelViewMatrix();

	glMatrixMode(GL_MODELVIEW);
    glLoadIdentity() ;

	mProgram->setLightNum(SceneManager::getInstance()->getScene()->getLights().size());
	mProgram->setIsShaded(shaderOn);
	mProgram->setIsLight(lightOn);
	mProgram->setIsTex(texOn);
	//mProgram->setIsDisplacementMap(disOn);

	mat4 sc(1.0) , tr(1.0) ; 
	sc = Transform::scale(mSx, mSy, 1.0) ; 
	tr = Transform::translate(mTx, mTy, 0.0) ; 

	// Multiply the matrices, accounting for OpenGL and GLM.
	sc = glm::transpose(sc) ; 
	tr = glm::transpose(tr) ; 
	mat4 transf = modelViewMatrix * tr * sc; // TO_SCALE, then TO_TRANSLATE, then lookat.

	mScene->render(transf);

	glLoadMatrixf(glm::value_ptr(modelViewMatrix));
	mProgram->setLights(mScene->getLights(), modelViewMatrix);

	glutSwapBuffers();
}

void SceneManager::handleIdle() {
	handleKeyboard();

	if (mIsRunning) {
		for (int i = 0; i < mScene->getEntities().size(); i++) {
			mScene->getEntities()[i]->update(0);
		}
	}

	glutPostRedisplay();
}

void SceneManager::handleReshape(int width, int height) {
	mScene->setSize(width, height);

	float aspect = mScene->getWidth() / (float) mScene->getHeight();

	mat4 mv ; // just like for lookat
	glMatrixMode(GL_PROJECTION);
	// I am changing the projection stuff to be consistent with lookat
	mv = glm::perspective(mScene->getCamera()->getFov()*mZoomFactor, aspect, mScene->getCamera()->getZMin(), mScene->getCamera()->getZMax()); 

	glLoadMatrixf(glm::value_ptr(mv)) ; 

	glViewport(0, 0, mScene->getWidth(), mScene->getHeight());
}

void SceneManager::handleKeyboard() {
	if (mInputManager->isKeyDown(100)) {
		// Left
		if (TransformOp == TO_SCALE)
			mSx -= mAmount * 0.01 ; 
		else if (TransformOp == TO_TRANSLATE)
			mTx -= mAmount * 0.01 ; 
	}
	if (mInputManager->isKeyDown(101)) {
		// Up
		if (TransformOp == TO_SCALE)
			mSy -= mAmount * 0.01 ; 
		else if (TransformOp == TO_TRANSLATE)
			mTy -= mAmount * 0.01 ; 
	}
	if (mInputManager->isKeyDown(102)) {
		// Right
		if (TransformOp == TO_SCALE)
			mSx += mAmount * 0.01 ; 
		else if (TransformOp == TO_TRANSLATE)
			mTx += mAmount * 0.01 ; 
	}
	if (mInputManager->isKeyDown(103)) {
		// Down
		if (TransformOp == TO_SCALE)
			mSy -= mAmount * 0.01 ; 
		else if (TransformOp == TO_TRANSLATE)
			mTy -= mAmount * 0.01 ; 
	}

	if (mInputManager->consumeKeyPress('+')) {
		mAmount *= 1.1;
		std::cout << "mAmount set to " << mAmount << "\n" ;
	}
	if (mInputManager->consumeKeyPress('-')) {
		mAmount /= 1.1;
		std::cout << "mAmount set to " << mAmount << "\n" ; 
	}
	if (mInputManager->isKeyDown('i')) {
		mZoomFactor += 0.5;
		if (mZoomFactor > mMaxZoom)
			mZoomFactor = mMaxZoom;
		handleReshape(mScene->getWidth(),mScene->getHeight());
	}
	if (mInputManager->isKeyDown('o')) {
		mZoomFactor -= 0.5;
		if (mZoomFactor <= 0)
			mZoomFactor = mMinZoom;

		handleReshape(mScene->getWidth(),mScene->getHeight());
	}
	if (mInputManager->consumeKeyPress('h')) {
		printHelp();
	}
	if (mInputManager->isKeyDown(27)) {
		exit(0) ;
	}
	if (mInputManager->consumeKeyPress(',')) {
		mIsRotate = !mIsRotate;
		cout << "Looking around by mouse is turned to " << (mIsRotate? "true" : "false") << "\n";
	}
	if (mInputManager->consumeKeyPress('v')) {
		TransformOp = TO_VIEW ;
		std::cout << "Operation is set to View\n" ; 
	}
	if (mInputManager->consumeKeyPress('t')) {
		TransformOp = TO_TRANSLATE ; 
		std::cout << "Operation is set to TO_TRANSLATE\n" ; 
	}
	if (mInputManager->consumeKeyPress('b')) {
		shaderOn = !shaderOn;
		std::cout << "Shader is turned " << (shaderOn? "true" : "false") << "\n";
	}
	if (mInputManager->consumeKeyPress('l')) {
		lightOn = !lightOn;
		cout << "Light is turned " << (lightOn? "true" : "false") << "\n";
	}
	if (mInputManager->isKeyDown('w')) {
		mScene->getCamera()->moveForward(mAmount*0.2);
	}
	if (mInputManager->isKeyDown('s')) {
		mScene->getCamera()->moveBackward(mAmount*0.2);
	}
	if (mInputManager->consumeKeyPress('t')) {
		texOn = !texOn;
		std::cout << "Texture is turned " << (texOn? "true" : "false") << "\n";
	}
	if (mInputManager->isKeyDown('a')) {
		mScene->getCamera()->moveLeft(mAmount*0.2);
	}
	if (mInputManager->isKeyDown('d')) {
		mScene->getCamera()->moveRight(mAmount*0.2);
	}
	if (mInputManager->consumeKeyPress('p')) {
		Model* model = mScene->getAssetManager()->getModel("pig1");
		model->mDisplayWireframe= !model->mDisplayWireframe;
	}
	if (mInputManager->consumeKeyPress('u')) {
		mIsRunning = !mIsRunning;
	}

	//if (key >= '0' && key <= '9')
	//{
	//	int temp = (int)(key - 48);

	//	if (temp >= mScene->getLights().size())
	//	{
	//		TransformOp = TO_VIEW;
	//		cerr << "There is no light " << temp << ". Please choose light from 0 to " << mScene->getLights().size() - 1 << endl
	//			<< "Operation is set back to View." << endl;
	//	}
	//	else{
	//		lightId = temp;
	//		cout << "Operation is set to Light " << lightId << "\n";
	//		TransformOp = TO_LIGHTOP;
	//	}
	//	break;
	//}

	glutPostRedisplay();
}

void SceneManager::handlePassiveMouse(int x, int y){
	if (!mWarped) {
		if (mCenterX != x) {
			mScene->getCamera()->yaw((x - mCenterX) / 1000.0);
		}
		if (mCenterY != y) {
			mScene->getCamera()->pitch((y - mCenterY) /  1000.0);
		}
		mWarped = true;
		glutWarpPointer(mCenterX, mCenterY);
	} else {
		mWarped = false;
	}
	glutPostRedisplay();
}

void SceneManager::handleMouseDrag(int x, int y) {
	handlePassiveMouse(x, y);
}

void SceneManager::handleMouse(int button, int state, int x, int y){
}

void SceneManager::printHelp() {
	cout << "\npress 'h' to print this message again.\n" 
		<< "press '+' or '-' to change the mAmount of rotation that\noccurs with each arrow press.\n" 
		<< "press 'g' to switch between using glm::lookAt and glm::Perspective or your own LookAt.\n"       
		<< "press 'r' to reset the transformations.\n"
		<< "press 'v' 't' 's' to do View [default], TO_TRANSLATE, TO_SCALE.\n"
		<< "press ESC to quit.\n" ;  
}

