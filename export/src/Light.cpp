#include "Light.h"
#include "Transform.h"

Light::Light(float x, float y, float z, float w, float r, float g, float b, float a) {
	mPosition = vec4(x, y, z, w);
	mInitialPosition = mPosition;
	mColor = vec4(r, g, b, a);
	mIsUpSet = false;
}
	
Light::~Light(void)
{
}

void Light::transformUp(float degree, vec3 cameraUp)
{
	vec3 lightDir = vec3(mPosition.x, mPosition.y, mPosition.z);

	if (!mIsUpSet)
	{
		mIsUpSet = true;
		mUp = cameraUp;
	}

	Transform::up(degree, lightDir, mUp);

	mPosition.x = lightDir.x;
	mPosition.y = lightDir.y;
	mPosition.z = lightDir.z;
}

void Light::transformLeft(float degree, vec3 cameraUp)
{
	vec3 lightDir = vec3(mPosition.x, mPosition.y, mPosition.z);

	if (!mIsUpSet)
	{
		mIsUpSet = true;
		mUp = cameraUp;
	}

	Transform::left(degree, lightDir, mUp);

	mPosition.x = lightDir.x;
	mPosition.y = lightDir.y;
	mPosition.z = lightDir.z;
}

vec4 Light::getPosition() const{
	return mPosition;
}

void Light::setPosition(vec4 pos){
	mPosition = pos;
}

vec4 Light::getColor() const {
	return mColor;
}
void Light::setColor(vec4 color){
	mColor = color;
}

vec3 Light::getUp() const{
	return mUp;
}

void Light::setUp(vec3 up){
	mUp = up;
}

bool Light::getIsUpSet() const{
	return mIsUpSet;	
}

void Light::setIsUp(bool isUp){
	mIsUpSet = isUp;
}