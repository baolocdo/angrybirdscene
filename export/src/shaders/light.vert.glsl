# version 120 

uniform sampler2D displacementMap;
uniform int isDisplacementMap;

// Mine is an old machine.  For version 130 or higher, do 
// out vec4 color ;  
// out vec4 mynormal ; 
// out vec4 myvertex ;
// That is certainly more modern

varying vec4 color ; 
varying vec3 mynormal ; 
varying vec4 myvertex ; 

void main() {
	vec4 newVertexPos;
	vec4 dv;

	gl_TexCoord[0] = gl_MultiTexCoord0 ; 
	
	if (isDisplacementMap > 0) {
		dv = texture2D( displacementMap,  gl_TexCoord[0].st ) - vec4(0.5);
		newVertexPos = vec4(gl_Normal * dv.x * 0.25, 0.0) + gl_Vertex;
		gl_Position = gl_ProjectionMatrix * gl_ModelViewMatrix * newVertexPos;
	} else {
		gl_Position = gl_ProjectionMatrix * gl_ModelViewMatrix * gl_Vertex ;
	}
	color = gl_Color ; 
	mynormal = gl_Normal ; 
	myvertex = gl_Vertex ; 
}

