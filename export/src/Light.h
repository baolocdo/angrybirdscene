#pragma once

#include "stdafx.h"

class Light
{
public:
	Light(float x, float y, float z, float w, float r, float g, float b, float a);
	~Light(void);

	void transformLeft(float degree, vec3 cameraUp);
	void transformUp(float degree, vec3 cameraUp);

	vec4 getPosition() const;
	void setPosition(vec4);

	vec4 getColor() const;
	void setColor(vec4);

	vec3 getUp() const;
	void setUp(vec3);

	bool getIsUpSet() const;
	void setIsUp(bool);

private:
	vec3 mUp;
	vec4 mPosition;
	vec4 mColor;

	vec4 mInitialPosition;
	bool mIsUpSet;
};
