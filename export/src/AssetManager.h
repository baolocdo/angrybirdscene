#pragma once

#include "stdafx.h"
#include "Model.h"
#include "GlmModel.h"
#include "ObjModel.h"
#include "RawModel.h"

class AssetManager
{
public:
	AssetManager(void);
	~AssetManager(void);

	RawModel* loadRawModel(string name, string file);
	ObjModel* loadObjModel(string name, string file);
	GlmModel* loadGlmModel(string name, string file);
	GlmModel* loadGlmModel(string name, string file, string tgaFile);
	GlmModel* loadGlmModel(string name, string file, string tgaFile, bool repeat);
	GlmModel* loadGlmModel(string name, string file, string tgaFile, string mapFile);
	Model* getModel(string name);
	void loadMaterialLib(string file);
	Texture* loadTexture(string file);
	GLuint loadTexture(string file, bool repeat);
private:
	map<string, Texture*> mTextures;
	map<string, Material*> mMaterials;
	map<string, Model*> mModels;
};