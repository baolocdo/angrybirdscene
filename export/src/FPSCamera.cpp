#include "stdafx.h"

#include "FPSCamera.h"
#include "Quaternion.h"

FPSCamera::FPSCamera(vec3 position, vec3 lookAt, float fov)
{
	mPosition = position;
	mLookAt  = lookAt;
	mUp = vec3(0.0, 1.0, 0.0);
	mFov = fov;
	mZMin = 2.0;
	mZMax = 500.0;
	mInitialPosition = position;
	mInitialLookAt = lookAt;

	mLeft = glm::normalize(glm::cross(mPosition - mLookAt, mUp));
	mInitialUp = mUp;
}

FPSCamera::FPSCamera(vec3 position, vec3 lookAt, float fov, float zMin, float zMax)
{
	mPosition = position;
	mLookAt  = lookAt;
	mUp = vec3(0.0, 1.0, 0.0);
	mFov = fov;
	mZMin = zMin;
	mZMax = zMax;
	mInitialPosition = position;
	mInitialLookAt = lookAt;

	mLeft = glm::normalize(glm::cross(mPosition - mLookAt, mUp));
	mInitialUp = mUp;
}

FPSCamera::~FPSCamera(void)
{
}

void FPSCamera::yaw(float degree) {
	if (degree < 1) {
		mLookAt = Quaternion(-degree, mUp) * (mLookAt - mPosition) + mPosition;
		mLeft = glm::normalize(glm::cross(mPosition - mLookAt, mUp));
	}
}
void FPSCamera::pitch(float degree) {
	if (degree < 1) {
		mLookAt = Quaternion(degree, mLeft) * (mLookAt - mPosition) + mPosition;
	}
}
void FPSCamera::roll(float degree) {
}