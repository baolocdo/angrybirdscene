#pragma once

#include "stdafx.h"

#include "shaders.h"
#include <iostream>
#include <fstream>

GLdouble eyeloc = 2.0 ; // Where to look from; initially 0 -2, 2
GLuint vertexshader, fragmentshader, shaderprogram ; // shaders

GLubyte woodtexture[256][256][3] ; // ** NEW ** texture (from grsites.com)
GLuint texNames[1] ; // ** NEW ** texture buffer

GLuint buffers[4] ; // ** NEW ** List of buffers for geometric data 
// ** NEW ** Floor Geometry is specified with a vertex array
// ** NEW ** Same for other Geometry 
// The Buffer Offset Macro is from Red Book, page 103, 106

#define BUFFER_OFFSET(bytes) ((GLubyte *) NULL + (bytes))
#define NumberOf(array) (sizeof(array)/sizeof(array[0])) 

const GLfloat floorverts[4][3] = {
	{0.5, 0.5, 0.0}, {-0.5, 0.5, 0.0}, {-0.5, -0.5, 0.0}, {0.5, -0.5, 0.0}
} ; 
//const GLfloat floorcol[4][3] = {
//	{1.0, 1.0, 1.0}, {1.0, 1.0, 1.0}, {1.0, 1.0, 1.0}, {1.0, 1.0, 1.0}
//} ; 
const GLubyte floorinds[1][4] = { {0, 1, 2, 3} } ; 
const GLfloat floortex[4][2] = { 
	{1.0, 1.0}, {0.0, 1.0}, {0.0, 0.0}, {1.0, 0.0}
} ;

// This function takes in a vertex, color, index and type array 
// And does the initialization for an object.  
// The partner function below it draws the object 

void initobject(GLfloat * vert, GLint sizevert, GLubyte * inds, GLint sizeind) {
	glBindBuffer(GL_ARRAY_BUFFER, buffers[0]) ; 
	glBufferData(GL_ARRAY_BUFFER, sizevert, vert,GL_STATIC_DRAW);
	//glBindBuffer(GL_ARRAY_BUFFER, buffers[1]) ; 
	//glBufferData(GL_ARRAY_BUFFER, sizecol, col,GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[2]) ; 
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeind, inds,GL_STATIC_DRAW);
}

// Very basic code to read a ppm file
// And then set up buffers for texture coordinates
void inittexture (const char * filename) {
	int i,j,k ;
	FILE * fp ; 
	GLint err ; 
	assert(fp = fopen(filename,"rb")) ;
	fscanf(fp,"%*s %*d %*d %*d%*c") ;
	for (i = 0 ; i < 256 ; i++)
		for (j = 0 ; j < 256 ; j++)
			for (k = 0 ; k < 3 ; k++)
				fscanf(fp,"%c",&(woodtexture[i][j][k])) ;
	fclose(fp) ;  

	// Set up Texture Coordinates
	glGenTextures(1, texNames) ; 

	glBindBuffer(GL_ARRAY_BUFFER, buffers[3]) ; 
	glBufferData(GL_ARRAY_BUFFER, sizeof (floortex), floortex,GL_STATIC_DRAW);

	glBindTexture (GL_TEXTURE_2D, texNames[0]) ; 
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGB, 256, 256, 0, GL_RGB, GL_UNSIGNED_BYTE, woodtexture) ;
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR) ; 
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR) ; 
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT) ;
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT) ;
}
// And a function to draw with textures, similar to drawobject
void drawtexture(GLuint texture) {
	glBindBuffer(GL_ARRAY_BUFFER, buffers[0]) ; 
	glVertexPointer(3, GL_FLOAT, 0, BUFFER_OFFSET(0)) ; 
	// Even with texturing, so we can blend if needed.
	//glBindBuffer(GL_ARRAY_BUFFER, buffers[1]) ; 
	//glColorPointer(3, GL_FLOAT, 0, BUFFER_OFFSET(0)) ; 

	glBindTexture(GL_TEXTURE_2D, texture) ; 
	glBindBuffer(GL_ARRAY_BUFFER, buffers[3]) ; // Set texcoords
	glTexCoordPointer(2, GL_FLOAT, 0, BUFFER_OFFSET(0)) ; 

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[2]) ; 
	glDrawElements(GL_POLYGON, sizeof(floorinds), GL_UNSIGNED_BYTE, BUFFER_OFFSET(0)) ; 

}

/* New helper transformation function to transform vector by modelview */ 
void transformvec (const GLfloat input[4], GLfloat output[4]) {
	GLfloat modelview[16] ; // in column major order
	glGetFloatv(GL_MODELVIEW_MATRIX, modelview) ; 

	for (int i = 0 ; i < 4 ; i++) {
		output[i] = 0 ; 
		for (int j = 0 ; j < 4 ; j++) 
			output[i] += modelview[4*j+i] * input[j] ; 
	}
}

void display(void)
{
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT) ; 

	drawtexture(texNames[0]) ; // Texturing floor 

	glutSwapBuffers() ; 
}

/* Reshapes the window appropriately */
void reshape(int w, int h)
{
	glViewport (0, 0, (GLsizei) w, (GLsizei) h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Think about the rationale for this choice for gluPerspective 
	// What would happen if you changed near and far planes? 
	gluPerspective(30.0, (GLdouble)w/(GLdouble)h, 1.0, 10.0) ;

}


void init (void) 
{
	/* select clearing color 	*/
	glClearColor (0.0, 0.0, 0.0, 0.0);

	/* initialize viewing values  */
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Think about this.  Why is the up vector not normalized?
	glMatrixMode(GL_MODELVIEW) ;
	glLoadIdentity() ;

	gluLookAt(0,-eyeloc,eyeloc,0,0,0,0,1,1) ;

	glEnableClientState(GL_VERTEX_ARRAY) ; 
	//glEnableClientState(GL_COLOR_ARRAY) ; 
	glEnableClientState(GL_TEXTURE_COORD_ARRAY) ; 

	vertexshader = initshaders(GL_VERTEX_SHADER, "shaders/light.vert.glsl") ;
	fragmentshader = initshaders(GL_FRAGMENT_SHADER, "shaders/light.frag.glsl") ;
	shaderprogram = initprogram(vertexshader, fragmentshader) ; 
	
	// Define a sampler.  See page 709 in red book, 7th ed.
	glUniform1i(glGetUniformLocation(shaderprogram, "tex"), 0) ; // Could also be GL_TEXTURE0 
	glUniform1i(glGetUniformLocation(shaderprogram,"isTex"), true) ; 

	glGenBuffers(4, buffers) ; // 1 for texcoords 

	inittexture("wood.ppm") ; 

	initobject((GLfloat *) floorverts, sizeof(floorverts), (GLubyte *) floorinds, sizeof (floorinds)) ; 
	glEnable(GL_DEPTH_TEST) ;
}

//int main(int argc, char** argv)
//{
//	glutInit(&argc, argv);
//
//	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
//
//	glutInitWindowSize (500, 500); 
//	glutInitWindowPosition (100, 100);
//	glutCreateWindow ("Simple Demo with Shaders");
//
//	GLenum err = glewInit() ; 
//	if (GLEW_OK != err) { 
//		std::cerr << "Error: " << glewGetString(err) << std::endl; 
//	} 
//
//	init (); // Always initialize first
//
//	// Now, we define callbacks and functions for various tasks.
//	glutDisplayFunc(display); 
//	glutReshapeFunc(reshape) ;
//
//	glutMainLoop(); // Start the main code
//	return 0;   /* ANSI C requires main to return int. */
//}

