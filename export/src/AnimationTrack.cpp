#include "AnimationTrack.h"


AnimationTrack::AnimationTrack(float amount)
	: mAmount(amount)
{
}


AnimationTrack::~AnimationTrack(void)
{
}

void AnimationTrack::attachEntity(Entity* entity) {
	mEntity = entity;
	reset();
}

void AnimationTrack::update(float timeElapsed) {
	mEntity->rotate(mAmount, vec3(0, 1, 0));

	//AnimationFrame* frame1 = *mCurrentFrameIterator;
	//AnimationFrame* frame2 = *mNextFrameIterator;

	//mTime += timeElapsed;
	//if (mTime > (*mCurrentFrameIterator)->mTime) {
	//	mCurrentFrameIterator++;
	//	mNextFrameIterator++;
	//	frame1 = frame2;
	//	frame2 =  *mNextFrameIterator;
	//}

	// TODO: calculate the new position for entity
}

void AnimationTrack::reset() {
	//mCurrentFrameIterator = mAnimationFrames.begin();
	//mNextFrameIterator = mCurrentFrameIterator + 1;
}