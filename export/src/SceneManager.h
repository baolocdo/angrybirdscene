#pragma once
#include "stdafx.h"

#include "GpuProgram.h"
#include "Scene.h"
#include "InputManager.h"

#include <stack>

const int INIT_WINDOW_POSITION_X = 200;
const int INIT_WINDOW_POSITION_Y = 200;

const int DEFAULT_WINDOW_HEIGHT = 500;
const int DEFAULT_WINDOW_WIDTH = 500;

const vec3 ORIGIN = vec3(0.0, 0.0, 0.0);

static enum {
	TO_VIEW,
	TO_SCALE,
	TO_TRANSLATE,
	TO_LIGHTOP,
} TransformOp;

class SceneManager
{
public:
	SceneManager(void);
	~SceneManager(void);

	static SceneManager* getInstance();

	void initWindow(string title);
	void initWindow(string title, int posX, int posY);

	void load(string file, string vertexShader, string fragmentShader);
	void run();
	void pause();
	void printHelp();

	void handleRender();
	void handleIdle();
	void handleKeyboard();
	void handleReshape(int width, int height);
	void handlePassiveMouse(int x, int y);
	void handleMouse(int, int, int, int);
	void handleMouseDrag(int x, int y);
	void handleMouseEntry(int state);
	void handleActiveMouse(int x, int y);

	Scene* getScene() const {
		return mScene;
	}

	GpuProgram* getGpuProgram() const {
		return mProgram;
	}
	
	InputManager* getInputManager() const {
		return mInputManager;
	}

	float getLastTimeFrame() {return mLastTimeFrame;}
	float getTimeElasped() {return mTimeElapsed;}

	// Screen movement
	int getMoveSpeed() {return mMoveSpeed;}
	int getAmount() {return mAmount;}

	// Zoom:
	float getZoomFactor() {return mZoomFactor;}
	float getMaxZoom() {return mMaxZoom;}
	float getMinZoom() {return mMinZoom;}

	// Mouse:
	bool isWarped() {return mWarped;}
	bool isRotate() {return mIsRotate;}

	//The middle point
	int getCenterX() {return mCenterX;}
	int getCenterY() {return mCenterY;}

private:
	static SceneManager* mInstance;

	InputManager* mInputManager;
	GpuProgram* mProgram;
	Scene* mScene;
	//SceneNode* mRoot;

	// handle time
	float mLastTimeFrame;
	float mTimeElapsed;

	// Screen movement
	int mMoveSpeed;
	float mAmount;

	// Zoom:
	float mZoomFactor;
	float mMaxZoom;
	float mMinZoom;

	// Mouse:
	bool mWarped;
	bool mIsRotate;

	//The middle point
	int mCenterX;
	int mCenterY;

	float mSx;
	float mSy;
	float mTx;
	float mTy;

	int lightId;

	//Turn on/off light and tex
	bool lightOn;
	bool shaderOn;
	bool texOn;
	bool disOn;

	bool mIsRunning;
};

