#include "stdafx.h"
#include <iostream>

#include "InputManager.h"

InputManager::InputManager(void)
{
	for (int i = 0; i < 256; i++) {
		mKeyboard[i] = KeyState::KS_UP;
	}
}

InputManager::~InputManager(void)
{
}

void InputManager::onKeyDown(int key, int x, int y) {
	cout << "key down " << key;
	mKeyboard[key] = KeyState::KS_DOWN;
}

void InputManager::onKeyUp(int key, int x, int y) {
	cout << "key up " << key;
	if (mKeyboard[key] == KeyState::KS_DOWN) {
		mKeyboard[key] = KeyState::KS_PRESSED;
	} else {
		mKeyboard[key] = KeyState::KS_UP;
	}
}

bool InputManager::isKeyDown(int key) {
	return mKeyboard[key] == KeyState::KS_DOWN;
}

bool InputManager::isKeyPressed(int key) {
	return mKeyboard[key] == KeyState::KS_PRESSED;
}

bool InputManager::consumeKeyPress(int key) {
	bool ret = (mKeyboard[key] == KeyState::KS_PRESSED);
	if (ret)
		mKeyboard[key] = KeyState::KS_UP;
	return ret;
}