#include "ObjModel.h"
#include "SceneManager.h"

ObjModel::ObjModel(void)
{
}


ObjModel::~ObjModel(void)
{
	// TODO: unload resources
}

void ObjModel::scaleToUnit() {
	GLuint  i;
    float maxx, minx, maxy, miny, maxz, minz;
    float cx, cy, cz, w, h, d;
    float scale;
    
    /* get the max/mins */
	maxx = minx = mVertices[0].x;
    maxy = miny = mVertices[0].y;
    maxz = minz = mVertices[0].z;
	for (i = 0; i < mVertices.size(); i++) {
        if (maxx < mVertices[i].x)
            maxx = mVertices[i].x;
        if (minx > mVertices[i].x)
            minx = mVertices[i].x;
        
        if (maxy < mVertices[i].y)
            maxy = mVertices[i].y;
        if (miny > mVertices[i].y)
            miny = mVertices[i].y;
        
        if (maxz < mVertices[i].z)
            maxz = mVertices[i].z;
        if (minz > mVertices[i].z)
            minz = mVertices[i].z;
    }
    
    /* calculate model width, height, and depth */
	w = glm::abs(maxx) + glm::abs(minx);
    h = glm::abs(maxy) + glm::abs(miny);
    d = glm::abs(maxz) + glm::abs(minz);
    
    /* calculate center of the model */
    cx = (maxx + minx) / 2.0;
    cy = (maxy + miny) / 2.0;
    cz = (maxz + minz) / 2.0;
    
    /* calculate unitizing scale factor */
	float max = w > h ? w : h;
	max = max > d ? max : d;
    scale = 2.0 / max;
    
    /* translate around center then scale */
	for (i = 0; i < mVertices.size(); i++) {
		mVertices[i] -= vec3(-cx, -cy, -cz);
		mVertices[i] *= scale;
    }
}

void ObjModel::render() {
	SceneManager::getInstance()->getGpuProgram()->setIsTex(true);
	SceneManager::getInstance()->getGpuProgram()->setTex(GL_TEXTURE0);

	map<string, Mesh*>::const_iterator it;
	for(it = mMeshes.begin(); it != mMeshes.end(); ++it) {
		Mesh* mesh = (*it).second;
		mesh->render();
	}
}

//void Model::render() {
//	glEnable(GL_TEXTURE_2D);
//	glTexEnvf(GL_TEXTURE_ENV , GL_TEXTURE_ENV_MODE, GL_MODULATE);
//	map<string, Mesh*>::const_iterator it;
//
//	for(it = mMeshes.begin(); it != mMeshes.end(); ++it) {
//		Mesh* mesh = (*it).second;
//
//		if (mesh->mMaterial != NULL) {
//			if (mesh->mMaterial->mTexture) {
//				SceneManager::getInstance()->getGpuProgram()->setIsTex(true);
//				SceneManager::getInstance()->getGpuProgram()->setTex(GL_TEXTURE0);
//				glBindTexture(GL_TEXTURE_2D, mesh->mMaterial->mTexture->getID());
//			}
//			SceneManager::getInstance()->getGpuProgram()->setAmbient(mesh->mMaterial->mAmbient);
//			SceneManager::getInstance()->getGpuProgram()->setDiffuse(mesh->mMaterial->mDiffuse);
//			SceneManager::getInstance()->getGpuProgram()->setSpecular(mesh->mMaterial->mSpecular);
//			SceneManager::getInstance()->getGpuProgram()->setEmission(mesh->mMaterial->mEmission);
//			SceneManager::getInstance()->getGpuProgram()->setShininess(mesh->mMaterial->mShininess);
//		}
//
//		glBegin(GL_TRIANGLES);
//		for (int i = 0; i < mesh->mTriangles.size(); i++) {
//			Triangle* triangle = mesh->mTriangles[i];
//
//			glTexCoord2fv(glm::value_ptr(mTexCoords[triangle->getTIndices()[0]]));
//			glNormal3fv(glm::value_ptr(mNormals[triangle->getNIndices()[0]]));
//			glVertex3fv(glm::value_ptr(mVertices[triangle->getVIndices()[0]]));
//
//			glTexCoord2fv(glm::value_ptr(mTexCoords[triangle->getTIndices()[1]]));
//			glNormal3fv(glm::value_ptr(mNormals[triangle->getNIndices()[1]]));
//			glVertex3fv(glm::value_ptr(mVertices[triangle->getVIndices()[1]]));
//
//			glTexCoord2fv(glm::value_ptr(mTexCoords[triangle->getTIndices()[2]]));
//			glNormal3fv(glm::value_ptr(mNormals[triangle->getNIndices()[2]]));
//			glVertex3fv(glm::value_ptr(mVertices[triangle->getVIndices()[2]]));
//		}
//		glEnd();
//	}
//}
//
//map<string, Material*> Model::getMaterials() const {
//	return mMaterials;
//}
//
//Material* Model::getMaterial(int index)	{
//	return mMaterials[index];
//}
//
//vector<Mesh*> Model::getMeshes(){
//	return mMeshes;
//}
//
//Mesh* Model::getMesh(int index)	{
//	return mMeshes[index];
//}
