#include "Cube.h"

Cube::Cube(float size, mat4 transformMatrix, vec4 ambient, vec4 diffuse, vec4 specular, vec4 emission, float shininess)
	: Entity(size, transformMatrix, ambient, diffuse, specular, emission, shininess)
{
}


Cube::~Cube(void)
{
}

void Cube::render(mat4 modelViewMatrix) {
	Entity::render(modelViewMatrix);
	glutSolidCube(mSize);
}