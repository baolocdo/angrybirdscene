#include "Entity.h"

#include "SceneManager.h"
#include "GpuProgram.h"
#include "Transform.h"
#include "AnimationTrack.h"

Entity::Entity(float size, mat4 transformMatrix, vec4 ambient, vec4 diffuse, vec4 specular, vec4 emission, float shininess)
	: mSize(size)
	, mTransformMatrix(transformMatrix)
	, mAmbient(ambient)
	, mDiffuse(diffuse)
	, mSpecular(specular)
	, mEmission(emission)
	, mShininess(shininess)
	, mTranslateMatrix(mat4(0.1))
	, mScaleMatrix(mat4(0.1))
	, mRotateMatrix(mat4(0.1))
	, mAnimation(NULL)
{
}

Entity::Entity(void) {
}

Entity::~Entity(void) {
}
void Entity::render(mat4 modelViewMatrix) {
	GpuProgram* shader = SceneManager::getInstance()->getGpuProgram();
	shader->setAmbient(mAmbient);
	shader->setDiffuse(mDiffuse);
	shader->setSpecular(mSpecular);
	shader->setEmission(mEmission);
	shader->setShininess(mShininess);
	glLoadMatrixf(&(modelViewMatrix * mScaleMatrix * mRotateMatrix * mTranslateMatrix * mTransformMatrix)[0][0]);
}

void Entity::update(float timeElapsed) {
	if (mAnimation) {
		mAnimation->update(timeElapsed);
	}
}

void Entity::attachAnimation(AnimationTrack* animation) {
	mAnimation = animation;
	mAnimation->attachEntity(this);
}

void Entity::translate(int tx, int ty)
{
	mTranslateMatrix *= Transform::translate(tx, ty, 0);
}

void Entity::rotate(float degree, vec3 axis)
{

	mTransformMatrix *= Transform::rotate(axis, glm::radians(degree));
}

void Entity::scale(int sx, int sy)
{
	mTransformMatrix *= Transform::scale(sx, sy, 1);
}

float Entity::getSize() const{
	return mSize;
}

void Entity::setSize(float size){
	mSize = size;
}

mat4 Entity::getScaleMatrix() const{
	return mScaleMatrix;
}

void Entity::setScaleMatrix(mat4 scale){
	mScaleMatrix = scale;
}

mat4 Entity::getRotateMatrix() const{
	return mRotateMatrix;
}

void Entity::setRotateMatrix(mat4 rotateMatrix){
	mRotateMatrix = rotateMatrix;
}

mat4 Entity::getTranslateMatrix() const{
	return mTranslateMatrix;	
}

void Entity::setTranslateMatrix(mat4 translateMatrix){
	mTranslateMatrix = translateMatrix;
}

mat4 Entity::getTransformMatrix() const{
	return mTransformMatrix;
}

void Entity::setTransformMatrix(mat4 transformMatrix){
	mTransformMatrix = transformMatrix;
}

vec4 Entity::getAmbient() const{
	return mAmbient;
}

void Entity::setAmbient(vec4 ambient){
	mAmbient = ambient;
}

vec4 Entity::getDiffuse() const{
	return mDiffuse;
}

void Entity::setDiffuse(vec4 diffuse){
	mDiffuse = diffuse;
}

vec4 Entity::getSpecular() const{
	return mSpecular;
}

void Entity::setSpecular(vec4 specular){
	mSpecular = specular;
}

vec4 Entity::getEmission() const{
	return mEmission;
}

void Entity::setEmission(vec4 emission) {
	mEmission = emission;
}

float Entity::getShininess() const{
	return mShininess;
}

void Entity::setShininess(float shininess){
	mShininess = shininess;
}
