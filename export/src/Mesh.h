#pragma once

#include "stdafx.h"
#include "Material.h"
#include "Triangle.h"

/** A Mesh is a group of faces with the same material. */
class Mesh
{
public:
	Mesh(string name);
	~Mesh(void);

	void generateVertexData(vector<vec3> vertices, vector<vec3> normals, vector<vec3> texCoords, vector<int> indices);
	void render();

	string mName;
	vector<Triangle*> mTriangles;
	Material* mMaterial;
	
	GLuint mBuffers[4];
	vector<vec3> mVerticesData;
	vector<vec3> mNormalsData;
	vector<vec3> mTexCoordsData;
	vector<int> mIndices;
};

