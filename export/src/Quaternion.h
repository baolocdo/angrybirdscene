#pragma once

#include "stdafx.h"

class Quaternion
{
public:
	inline Quaternion (float fW = 1.0, float fX = 0.0, float fY = 0.0, float fZ = 0.0) {
		w = fW;
		x = fX;
		y = fY;
		z = fZ;
	}
    /// Construct a quaternion from a rotation matrix
    inline Quaternion(const mat3& rot)
    {
        this->fromRotationMatrix(rot);
    }
    /// Construct a quaternion from an angle/axis
    inline Quaternion(const float rfAngle, const vec3& rkAxis)
    {
        this->fromAngleAxis(rfAngle, rkAxis);
    }
    /// Construct a quaternion from 3 orthonormal local axes
    inline Quaternion(const vec3& xaxis, const vec3& yaxis, const vec3& zaxis)
    {
        this->fromAxes(xaxis, yaxis, zaxis);
    }
    /// Construct a quaternion from 3 orthonormal local axes
    inline Quaternion(const vec3* akAxis)
    {
        this->fromAxes(akAxis);
    }
	/// Construct a quaternion from 4 manual w/x/y/z values
	inline Quaternion(float* valptr)
	{
		memcpy(&w, valptr, sizeof(float)*4);
	}

	void fromRotationMatrix (const mat3& kRot);
	void toRotationMatrix (mat3& kRot) const;

	void fromAngleAxis(const float& rfAngle, const vec3& rkAxis);
	void toAngleAxis(float rfAngle, vec3 rkAxis) const;

	void fromAxes(const vec3& xAxis, const vec3& yAxis, const vec3& zAxis);
	void fromAxes(const vec3* akAxis);
	void toAxes(vec3& xAxis, vec3& yAxis, vec3& zAxis) const;
	void toAxes(vec3* akAxis) const;

	vec3 xAxis(void) const;
	vec3 yAxis(void) const;
	vec3 zAxis(void) const;

    inline Quaternion& operator= (const Quaternion& rkQ)
	{
		w = rkQ.w;
		x = rkQ.x;
		y = rkQ.y;
		z = rkQ.z;
		return *this;
	}
	Quaternion operator+ (const Quaternion& rkQ) const;
    Quaternion operator- (const Quaternion& rkQ) const;
	Quaternion operator- () const;
    Quaternion operator* (const Quaternion& rkQ) const;
    Quaternion operator* (float fScalar) const;
	
	inline bool operator== (const Quaternion& rhs) const
	{
		return (rhs.x == x) && (rhs.y == y) &&
			(rhs.z == z) && (rhs.w == w);
	}
    inline bool operator!= (const Quaternion& rhs) const
	{
		return !operator==(rhs);
	}
	
	float Dot (const Quaternion& rkQ) const;  // dot product
	float Norm () const;  // squared-length
	/// Normalises this quaternion, and returns the previous length
	float normalize(void); 
	Quaternion Inverse () const;  // apply to non-zero quaternion
	Quaternion UnitInverse () const;  // apply to unit-length quaternion
	Quaternion Exp () const;
	Quaternion Log () const;

	vec3 operator* (const vec3 rkVector) const;

   	/** Calculate the local roll element of this quaternion.
	@param reprojectAxis By default the method returns the 'intuitive' result
		that is, if you projected the local Y of the quaternion onto the X and
		Y axes, the angle between them is returned. If set to false though, the
		result is the actual yaw that will be used to implement the quaternion,
		which is the shortest possible path to get to the same orientation and 
		may involve less axial rotation. 
	*/
	float getRoll(bool reprojectAxis = true) const;
   	/** Calculate the local pitch element of this quaternion
	@param reprojectAxis By default the method returns the 'intuitive' result
		that is, if you projected the local Z of the quaternion onto the X and
		Y axes, the angle between them is returned. If set to true though, the
		result is the actual yaw that will be used to implement the quaternion,
		which is the shortest possible path to get to the same orientation and 
		may involve less axial rotation. 
	*/

	float getPitch(bool reprojectAxis = true) const;
   	
	/** Calculate the local yaw element of this quaternion
	@param reprojectAxis By default the method returns the 'intuitive' result
		that is, if you projected the local Z of the quaternion onto the X and
		Z axes, the angle between them is returned. If set to true though, the
		result is the actual yaw that will be used to implement the quaternion,
		which is the shortest possible path to get to the same orientation and 
		may involve less axial rotation. 
	*/
	float getYaw(bool reprojectAxis = true) const;		
	
	/// Equality with tolerance (tolerance is max angle difference)
	bool equals(const Quaternion& rhs, const float tolerance) const;
		
	// spherical linear interpolation
    static Quaternion Slerp (float fT, const Quaternion& rkP,
        const Quaternion& rkQ, bool shortestPath = false);

    static Quaternion SlerpExtraSpins (float fT,
        const Quaternion& rkP, const Quaternion& rkQ,
        int iExtraSpins);

    // setup for spherical quadratic interpolation
    static void Intermediate (const Quaternion& rkQ0,
        const Quaternion& rkQ1, const Quaternion& rkQ2,
        Quaternion& rka, Quaternion& rkB);

    // spherical quadratic interpolation
    static Quaternion Squad (float fT, const Quaternion& rkP,
        const Quaternion& rkA, const Quaternion& rkB,
        const Quaternion& rkQ, bool shortestPath = false);

    // normalised linear interpolation - faster but less accurate (non-constant rotation velocity)
    static Quaternion nlerp(float fT, const Quaternion& rkP, 
        const Quaternion& rkQ, bool shortestPath = false);

    // cutoff for sine near zero
    static const float ms_fEpsilon;

    // special values
    static const Quaternion ZERO;
    static const Quaternion IDENTITY;
	
	float w, x, y, z;
};

