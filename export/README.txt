Loc Do (cs184-eb)
Thanh Hai Mai (cs184-cz)

This project is developed on Windows 7, VS 2010. Please contact us if you run into any compiling problem.
We also included the executable for your convenience.

Please see the latest version of README at
Website: locdo.info

Angry Bird Scene
	1> The scene is about an angry bird discovering the pigs have stolen her egg! She traces down them into their shelter. The scene is reasonably completed with trees, pigs, pigs' shelter, angry birds, stars (we will make it a game later), and a glyph on the wall to demo displacement mapping
	2> The brown start (non animated) is created by hand. The code for it is in 
	Scene::createStarByHand(). The star is a simple object, but it is indeed more interesting than a box.
	3> The pigs, the shelter, the angry bird, the trees are loaded from Maya obj file at run time.
	4> The animating stars are created from a raw file. We created the raw file by hand (it's a pain!).
	5> The scene is constructed using the file src/data/newScene.txt. We do not create the complete scene in a modeling program. Instead, we have our AssetManager load and our Scene class construct the entire scene.
	6> The egg, the skybox, the tree and the cave are textured.
	7> The egg are shiny, the shelter is dull.
	8> We have one directional light (line 29 in src/data/newScene.txt)
	9> We have several point lights (line 27-28 in src/data/newScene.txt)
	10> The pigs, the stars, the glyphs and the trees are instantiated more than one. They are not duplicated. The models are loaded once at the start, and the engine just draw a model  more than one time at different locations.
	11> Press 'P' to toogle wireframe for one pig.
	12> Most of my objects have normal, except the stars which are created by hand.
	13> We have double buffering, hidden surface elimination, and perspective projection.
	14> Press 'L' to toogle light for debug.
	15> Our program uses programmable shaders.
	16> We implemented displacement mapping in vertex shader. The glyphs on the side of the shelter are rendered from a plane and displaced the vertices.
	17> FPS camera is implemented. You can use W A S D and mouse to move.
	18> FPS camera is implemented. You can use W A S D and mouse to move.
	19> The stars are constantly rotated.
	
Extra Credit:
	1> Input Manager: buffered input for a smoother key detect.	
	2> First Person Camera: fully implemented.
	3> Skybox.