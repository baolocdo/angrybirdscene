AngryBirdScene
==============

Computer Graphics Customized Scene
This project is developed on Windows 7, VS 2010.

------------------------------------------------
What's in this program?
------------------------------------------------
The scene is about an angry bird discovering the pigs have stolen her egg! She traces down them into their shelter. 

The scene is reasonably completed with trees, pigs, pigs’ shelter, angry birds, stars (we will make it a game later), and a glyph on the wall to demo displacement mapping

 1. The pigs, the shelter, the angry bird, the trees are loaded from Maya obj file at run time.
 2. The pigs, the stars, the glyphs and the trees are instantiated more than one. They are not duplicated. The models are loaded once at the start, and the engine just draw a model  more than one time at different locations.
 3. Press ‘P’ to toggle wireframe for one pig
 4. We have double buffering, hidden surface elimination, and perspective projection.
 5. Press ‘L’ to toggle light for debug.
 6. Our program uses programmable shaders.
 7. We implemented displacement mapping in vertex shader. The glyphs on the side of the shelter are rendered from a plane and displaced the vertices.
 8. FPS camera is implemented. You can use W A S D and mouse to move.
 9. Credits: Other models including egg, crates, pigs and trees are from Turbosquid.com and thefree3dmodels.com

------------------------------------------------
The demo
------------------------------------------------
I uploaded on YouTube
http://www.youtube.com/watch?feature=player_embedded&v=8jgcDn-G7Jk


------------------------------------------------
How to run it?
------------------------------------------------
You will need to have GLUT and GLM to run this. 
GLM can be downloaded at http://glm.g-truc.net/
You also need to install these additional libraries: https://www.edx.org/c4x/BerkeleyX/CS184.1x/asset/homework_Additional_Libraries.zip

------------------------------------------------
FAQ to install the libraries above:
------------------------------------------------
(This is from EDX online course https://www.edx.org/courses/BerkeleyX/CS184.1x/2013_Spring/courseware/Unit_0/Homework_0/)

The skeleton code already includes most needed dependencies, and will compile without problems in the vast majority of cases. So, most of you will not need to read this FAQ at all.   Also, as a first step, if you are having difficulties, please update the drivers for your graphics card.  (It's amazing how many issues were fixed by this simple operation in the Fall EdX class).  As a general guideline, we do require support of OpenGL 2.1 and GLSL 120.  While almost any machine purchased in the last 5-6 years will support this, some integrated Intel graphics cards might not.  The only way to really test is to make sure homework 0 works. 

Please note that different computer systems produce slightly different images. We have set our thresholds in the autograder accordingly. Don't worry about a few pixels being off, as long as you pass the test.  Please also note that the autograder includes a visual watermark on the solution images, which will make them look slightly different from your own images, but will not affect the grading.  

If you still have difficulties passing the test and know your images are correct, please (1) Check the images are not zoomed (scaled) or resized in any way, (2) Turn off all antialiasing features for your graphics card; you may also need to set 3D anti-aliasing to Application Managed.

Windows

Visual Studio 2012 can be downloaded for free by students at https://www.dreamspark.com/. (You do need to be enrolled as a student in a regular academic program, and provide academic verification). If this free link does not work for you, we assume you can obtain Visual Studio or an equivalent C++ development environment by other means. The standard download is http://www.microsoft.com/visualstudio/eng/downloads. Choose the right version for your computer. Please note that the Visual Studio Express 2012 edition should be adequate for this course, and is free for everyone.

If you are having issues taking screenshots, you can either manually crop a print screen (printscreen button) or use Fraps to take a screenshot (the manual method may be harder for later assignments however). Some students have reported that this issue can be fixed simply by changing the code to save the back buffer instead of the front (this is in the third line of the saveScreenshot() procedure in mytest3.cpp), that is by using

glReadBuffer(GL_BACK);
(Only) if you run into issues with 32/64-bit compilation, make sure your platform target says Win32, rather than x64.

The instructions below apply only to users who cannot get the skeleton code to compile directly; most people will not need them.

Using Visual Studio, download: Additional_Libraries.zip

To import the required dependencies, copy from Additional_Libraries.zip:

Dll\glew32.dll              to          %SystemRoot%\system32
Dll\glut32.dll              to          %SystemRoot%\system32
Lib\glew32.lib              to          {VC Root}\Lib
Lib\glut32.lib              to          {VC Root}\Lib
Include\GL\glew.h           to          {VC Root}\Include\GL
Include\GL\glut.h           to          {VC Root}\Include\GL
%SystemRoot% on Windows 7 x64 is typically C:\Windows
{VC Root} for VS2008 is typically C:\Program Files (x86)\Microsoft Visual Studio 9.0\VC,
{VC Root} for VS2010 is typically C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0A\, 
{VC Root} for VS2012 is typically C:\Program Files (x86)\Windows Kits\8.0\include\um for includes 
and C:\Program Files (x86)\Windows Kits\8.0\um\x86 for libs.

If it still doesn't work, some students have reported that

Include\GL\glew.h           to          C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0A\Include\GL
Include\GL\glut.h           to          C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0A\Include\GL
Lib\glew32.lib              to          C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0A\lib
Lib\glut32.lib              to          C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0A\lib
Dll\glew32.dll              to          ...hw0\Debug
Dll\glut32.dll              to          ...hw0\Debug
Or

Dll\glew32.dll              to          C:\Windows\SysWOW64\
Dll\glut32.dll              to          C:\Windows\SysWOW64\
Help solve issues as well. Finally, open the .sln file.

The FreeImage Library .DLL is included with the Windows distribution. If you change your build configuration from "Debug" to "Release", you must copy "FreeImage.dll" to the "Release" directory that resides in the project root directory containing the .SLN file.

You may need to modify your project property sheet in VS2010 or earlier (although our tests indicate it is not required in VS 2008). This is not required in VS 2012, and for this reason we recommend you upgrade using the above free links. Before trying the fix below, some students have reported that they needed to change the PlatformToolset to V100 from V110 (and that this was sufficient). To do so, in VS 2010, go to Project Properties -> Configuration Properties -> General -> Platform Toolset field, and revert it to V100. If that works, great; if it doesn't solve the problem, read on. Changing the project property sheet can be done by the following steps:

Turn on Property Manager Box by clicking "View"->"Property Manager"
Right click on any debug configuration in the Property Manager and select "Add New Project Property Sheet"
You should save this sheet in a general folder because for next projects, you can just add the sheet you already created.
Open the newly created sheet by double click on it. The Property Page will appear.
Under C/C++ -> General, click on Additional Include Directories->Edit
Add the link to the Include folder in the Additional Include Directories form -> OK
If you are still having issues, update your drivers and try running HW1 or HW2 instead of HW0. If this works, then just use another machine for HW0 only. If HW1 and HW2 do not work, you may need to find another machine or OS. This only happens for a very small percentage of Windows systems; most are fine; we have also modified the skeletons recently to eliminate the vast majority of issues.

Linux

If you are using a synaptic-based distribution, like Ubuntu, run:

sudo apt-get install freeglut3-dev glew-utils libglew1.6-dev libfreeimage-dev
to install the required dependencies for our OpenGL assignments. If libglew1.6-dev cannot be found, try installing an older version, such as libglew1.5-dev. Also make sure your package manager is up to date.

To compile, finally run make inside the assignment directory. If you do not have make or g++, run:

sudo apt-get install build-essential
on your synaptic-based Linux to install them.

Some students recommend deleting the "lib" and "include" directories in the homework 0 directory, as you don't need them, provided you have appropriate system libraries installed (in some cases, the precompiled libraries included with the skeleton may also be incompatible with your system, in which case please go ahead and delete them in favor of the system libraries).

If you have compile errors from "usleep" not being in scope, try including the system file unistd.h in the includes for grader.h.

Please see the following advice on FreeBSD if you are having a problem: Just change the two /usr/X11R6/ to /usr/local/ in the Makefile, and run gmake. Install freeglut, freeimage and glew in /usr/ports/graphics, and remove the lib directory in the homework.

If you cannot get the pillars to display (the teapot works fine) in homework 0, and you've tried everything else (update the drivers etc.), a student pafnuty from the Fall EdX class has suggested this associated patch. Essentially, replace the pillar drawing routine with old-style OpenGL.  Please also test homeworks 1 and 2; they should work better since they don't use newer OpenGL vertex buffer objects.  

If you get a segmentation fault when saving the image, try adding glPixelStorei(GL_PACK_ALIGNMENT, 1); right before the glReadPixels call in saveScreenshot().   

OSX

Download and install Xcode from the App Store, or from http://developer.apple.com/xcode/ You may need an older version if you're on an older OSX version.

If using make, just run make in the assignment directory to compile. By default, neither make nor g++ are installed on OSX. To install them, first open Xcode. From the menu bar, navigate to "Xcode > Preferences". Next, choose the "Downloads" tab. Within Downloads, choose the "Components" tab. Click the "Install" button to the right of "Command Line Tools". This will install the neccessary build tools, such as make and g++.

If you wish to use Xcode's IDE, add GLUT framework (and maybe OpenGL framework also), and change include headers from GL/glut.h to GLUT/glut.h in main.cpp (or anywhere it appears). Please note that we do not support IDEs other than Visual Studio for Windows (we recommend the command line in OSX/Linux), so we will have limited ability to answer compilation questions for the XCode IDE, but you should be able to figure it out yourself, and with the help of other students.

You should not need to compile FreeImage for OSX; we provide it with the skeleton code. If you must install it on OSX, it would be simpler to use MacPorts or Homebrew to install the library than to compile it from source.

OSX: Mountain Lion

If HW0 won't compile, giving you an error like this (with dozens of lines of stuff after):

g++ -g -DGL_GLEXT_PROTOTYPES -I./include/ -I/usr/X11/include -DOSX -c mytest3.cpp
mytest3.cpp:15:21: error: GL/glut.h: No such file or directory
mytest3.cpp:21: error: 'GLdouble' does not name a type
mytest3.cpp:22: error: 'GLfloat' does not name a type
mytest3.cpp:23: error: 'GLfloat' does not name a type
....
Try installing XQuartz (download here). This is necessary because Apple stopped including X11 in Mountain Lion. Some students have reported that this step is not needed, and you can compile simply by changing the include GL/glut.h to GLUT/glut.h (some students also recommend changing GL/glext.h to OpenGL/glext.h ).

If the program has a problem finding the shaders directory, it's possible there is an issue with relative paths in Mountain Lion; try changing the path to the shaders to be the absolute path name within the program. (You will need to look in the skeleton to see where the path to the shaders is specified).  

Eclipse

You may want to browse this tutorial on compiling the homework for Eclipse.

If you encounter an error with

enum {FLOOR, CUBE} ;
It should be changed to:

const unsigned int FLOOR = 0 ;
const unsigned int CUBE = 1 ;
General

If the skeleton code does not render correctly, you may want to update your drivers. Based on past posts in the discussion forum, this is probably the single most common (and easily fixable) issue.

The code frameworks do require that your graphics card (GPU) support at least GLSL version 120 (OpenGL 2.1). While almost any NVIDIA or ATI graphics card will be suitable, some old integrated graphics cards (such as Intel GMA950 or similar) may not run the homeworks properly. Given the variety of systems, this should be taken as a guideline rather than absolute; the real test is if homework 0 does compile and work. If not, you may need to upgrade or find a different machine.

Different systems produce slightly different images. We have set our thresholds in the autograder accordingly. Don't worry about a few pixels being off, as long as you pass the test. If there's still a problem, please turn off all antialiasing options (see the top of the page).

Ignore compiler warnings if the program runs.

Be sure to activate floating behavior if you use a tiling WM (eg. DWM). If you don't, the screenshots will be skewed.

We do not directly offer support for IDEs other than Visual Studio on Windows. If you use XCode or Eclipse, you must generate the project yourself, and make sure it will still compile with the provided Makefiles (on OSX/Linux) or the Sln (on Windows), or create your own Makefile.

If you are getting the error:

ERROR: 0:9: error(#137) 'attribute' supported in vertex shaders only
ERROR: 0:10: error(#137) 'attribute' supported in vertex shaders only
ERROR: 0:11: error(#137) 'attribute' supported in vertex shaders only
Please replace the following lines in shaders/light.frag.glsl (lines 9 through 11):

attribute vec4 color ;
attribute vec3 mynormal ; 
attribute vec4 myvertex ; 
With:

varying vec4 color ;
varying vec3 mynormal ; 
varying vec4 myvertex ; 
