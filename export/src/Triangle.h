#pragma once

#include "stdafx.h"

class Triangle
{
public:
	Triangle();
	~Triangle();

	GLuint* getVIndices();           /* array of triangle vertex indices */
	void setVIndices(GLuint* vIndices);

	GLuint* getNIndices();           /* array of triangle normal indices */
	void setNIndices(GLuint* nIndices);

	GLuint* getTIndices();           /* array of triangle texcoord indices*/
	void setTIndices(GLuint* tIndices);

	GLuint getFIndex() const;                /* index of triangle facet normal */
	void setFIndex(GLuint fIndex);

	bool getVisible() const;
	void setVisible(bool visible);

private:
	GLuint mVIndices[3];           /* array of triangle vertex indices */
	GLuint mNIndices[3];           /* array of triangle normal indices */
	GLuint mTIndices[3];           /* array of triangle texcoord indices*/
	GLuint mFIndex;                /* index of triangle facet normal */
	bool mVisible;
};