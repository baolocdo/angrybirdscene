#pragma once
#include "Model.h"
#include "Material.h"
#include "glm.h"

class GlmModel :
	public Model
{
public:
	GlmModel(void);
	~GlmModel(void);

	virtual void scaleToUnit();
	virtual void render();

	GLMmodel* mGLMModel;
	GLuint mTextureID;
	bool mIsTextured;
	GLuint mDisplacementMapID;
	bool mIsDisplacementMap;
};

