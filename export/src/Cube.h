#pragma once

#include "Entity.h"

class Cube :
	public Entity
{
public:
	Cube(float size, mat4 transformMatrix, vec4 ambient, vec4 diffuse, vec4 specular, vec4 emission, float shininess);
	~Cube(void);
	virtual void render(mat4 modelViewMatrix);
};

