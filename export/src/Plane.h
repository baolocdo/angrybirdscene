#pragma once
#include "Entity.h"

class Plane	:
	public Entity
{
public:
	Plane(float size, mat4 transformMatrix, vec4 ambient, vec4 diffuse, vec4 specular, vec4 emission, float shininess);
	~Plane(void);
	virtual void render(mat4 modelViewMatrix);
};

