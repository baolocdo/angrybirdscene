#pragma once

#include "stdafx.h"
#include <fstream>

class FileTokenizer
{
public:
	FileTokenizer(string file);
	~FileTokenizer(void);

	vector<vector<string>> readAll();
private:
	string mFile;
	vector<string> tokenize(string str);
};

