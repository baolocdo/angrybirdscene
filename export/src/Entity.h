#pragma once

#include "stdafx.h"
//#include "AnimationTrack.h"

class AnimationTrack;

class Entity
{
public:
	Entity(float size, mat4 transformMatrix, vec4 ambient, vec4 diffuse, vec4 specular, vec4 emission, float shininess);
	Entity();
	~Entity(void);

	void attachAnimation(AnimationTrack* animation);

	virtual void render(mat4 modelViewMatrix);
	virtual void update(float timeElapsed);

	void translate(int tx, int ty);
	void scale(int sx, int sy);
	void rotate(float degree, vec3 axis);

	float getSize() const;
	void setSize(float size);

	mat4 getScaleMatrix() const;
	void setScaleMatrix(mat4);

	mat4 getRotateMatrix() const;
	void setRotateMatrix(mat4);

	mat4 getTranslateMatrix() const;
	void setTranslateMatrix(mat4);

	mat4 getTransformMatrix() const;
	void setTransformMatrix(mat4);

	vec4 getAmbient() const;
	void setAmbient(vec4);

	vec4 getDiffuse() const;
	void setDiffuse(vec4);

	vec4 getSpecular() const;
	void setSpecular(vec4);

	vec4 getEmission() const;
	void setEmission(vec4);

	float getShininess() const;
	void setShininess(float);

protected:
	AnimationTrack* mAnimation;

	float mSize;

	mat4 mScaleMatrix;
	mat4 mRotateMatrix;
	mat4 mTranslateMatrix;

	mat4 mTransformMatrix;
	vec4 mAmbient;
	vec4 mDiffuse;
	vec4 mSpecular;
	vec4 mEmission;
	float mShininess;
};

