#pragma once

#include "stdafx.h"
#include "Triangle.h"
#include "Material.h"
#include "Mesh.h"
#include "Texture.h"
#include "glm.h"
#include <map>

class Model
{
public:
	Model();
	~Model(void);

	virtual void scaleToUnit() = 0;
	virtual void render() = 0;
	Material* mMaterial;
	bool mDisplayWireframe;
};

