#pragma once

#include "stdafx.h"

class Texture
{
public:
	Texture(string file);
	~Texture();

	string getName() const;
	void setName(string name);

	GLuint getID();
	void setID(GLuint id);

	GLfloat getWidth() const;
	void setWidth(GLfloat width);

	GLfloat getHeight() const;
	void setHeight(GLfloat height);

private:
	string mName;
	GLuint mID;
	GLfloat mWidth;
	GLfloat mHeight;
};

