#include "Teapot.h"

Teapot::Teapot(float size, mat4 transformMatrix, vec4 ambient, vec4 diffuse, vec4 specular, vec4 emission, float shininess)
	: Entity(size, transformMatrix, ambient, diffuse, specular, emission, shininess)
{
}

Teapot::~Teapot(void)
{
}

void Teapot::render(mat4 modelViewMatrix) {
	Entity::render(modelViewMatrix);
	glutSolidTeapot(mSize);
}