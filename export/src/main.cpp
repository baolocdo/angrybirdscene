/*****************************************************************************/
/* HW3: Purple Angry Bird */
/* Loc Do (cs184-eb), Thanh Hai Mai (cs184-cz)   */
/*****************************************************************************/
#include "stdafx.h"
#include "SceneManager.h"

int main(int argc, char* argv[]) {
	glutInit(&argc, argv);
	// init
	SceneManager::getInstance()->initWindow("HW3: Angry Bird");
	// optional: load scene
	SceneManager::getInstance()->load("newScene.txt", "shaders/light.vert.glsl", "shaders/light.frag.glsl");
	// then run
	SceneManager::getInstance()->run();
	// user can pause with some key
	//SceneManager::getInstance()->pause();

	// received exit signal from user
	// clean up and returns
	delete SceneManager::getInstance();

	return 0;
}
