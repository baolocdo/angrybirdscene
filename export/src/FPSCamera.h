#pragma once

#include "Camera.h"

class FPSCamera :
	public Camera
{
public:
	FPSCamera(vec3 position, vec3 lookAt, float fov);
	FPSCamera(vec3 position, vec3 lookAt, float fov, float zMin, float zMax);
	~FPSCamera(void);

	virtual void yaw(float degree);
	virtual void pitch(float degree);
	virtual void roll(float degree);
};

