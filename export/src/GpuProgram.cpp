#include "GpuProgram.h"
#include "SceneManager.h"
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <sstream>
#include <iostream>
#include <fstream>

using namespace std;

GpuProgram::GpuProgram(string vertexShaderFile, string fragmentShaderFile)
{
	vertexshader = initShader(GL_VERTEX_SHADER, vertexShaderFile);
	fragmentshader = initShader(GL_FRAGMENT_SHADER, fragmentShaderFile);
	shaderprogram = initProgram(vertexshader, fragmentshader); 

	mIsLight =  glGetUniformLocation(shaderprogram,"isLight");
	mIsShaded = glGetUniformLocation(shaderprogram,"isShaded");
	mIsTex = glGetUniformLocation(shaderprogram, "isTex");
	mIsDisplacementMap = glGetUniformLocation(shaderprogram, "isDisplacementMap");

	mTex = glGetUniformLocation(shaderprogram, "tex");

	Scene* scene = SceneManager::getInstance()->getScene();
	vector<Light*> lights= scene->getLights();

	mLightPositions = new GLuint[lights.size()];
	mLightColor = new GLuint[lights.size()];

	for (int i = 0; i < lights.size(); i++)
	{
		stringstream temp;
		temp << i;

		string strPos = "lightposn[" + temp.str() + "]";
		string strColor = "lightcolor[" + temp.str() + "]";

		mLightPositions[i] = glGetUniformLocation(shaderprogram,  strPos.c_str());
		mLightColor[i] = glGetUniformLocation(shaderprogram, strColor.c_str());
	}

	mLightNum = glGetUniformLocation(shaderprogram,"lightNum");
	mAmbient = glGetUniformLocation(shaderprogram,"ambient") ;       
	mDiffuse = glGetUniformLocation(shaderprogram,"diffuse") ;       
	mSpecular = glGetUniformLocation(shaderprogram,"specular") ;       
	mEmission = glGetUniformLocation(shaderprogram,"emission") ;       
	mShininess = glGetUniformLocation(shaderprogram,"shininess") ;     
}


GpuProgram::~GpuProgram(void)
{
}

GLuint GpuProgram::initShader(GLenum type, string file){
	GLuint shader = glCreateShader(type) ; 
	GLint compiled ; 
	string str = readShader (file) ; 
	GLchar * cstr = new GLchar[str.size()+1] ; 
	const GLchar * cstr2 = cstr ; // Weirdness to get a const char
	strcpy(cstr,str.c_str()) ; 
	glShaderSource (shader, 1, &cstr2, NULL) ; 
	glCompileShader (shader) ; 
	glGetShaderiv (shader, GL_COMPILE_STATUS, &compiled) ; 
	if (!compiled) { 
		shaderError (shader) ; 
		throw 3 ; 
	}
	return shader ; 
}

GLuint GpuProgram::initProgram(GLuint vertexShader, GLuint fragmentShader){
	GLuint program = glCreateProgram() ; 
	GLint linked ; 
	glAttachShader(program, vertexshader) ; 
	glAttachShader(program, fragmentshader) ; 
	glLinkProgram(program) ; 
	glGetProgramiv(program, GL_LINK_STATUS, &linked) ; 
	if (linked) glUseProgram(program) ; 
	else { 
		programError(program) ; 
		throw 4 ; 
	}

	return program ; 
}

string GpuProgram::readShader(string filename){
	string str, ret = "" ; 
	ifstream in ; 
	in.open(filename) ; 
	if (in.is_open()) {
		getline (in, str) ; 
		while (in) {
			ret += str + "\n" ; 
			getline (in, str) ; 
		}
		//    cout << "Shader below\n" << ret << "\n" ; 
		return ret ; 
	}
	else {
		cerr << "Unable to Open File " << filename << "\n" ; 
		throw 2 ; 
	}
}

void GpuProgram::programError(const GLint program){
	GLint length ; 
	GLchar * log ; 
	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length) ; 
	log = new GLchar[length+1] ;
	glGetProgramInfoLog(program, length, &length, log) ; 
	cout << "Compile Error, Log Below\n" << log << "\n" ; 
	delete [] log ; 
}

void GpuProgram::shaderError(const GLint shader){
	GLint length ; 
	GLchar * log ; 
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length) ; 
	log = new GLchar[length+1] ;
	glGetShaderInfoLog(shader, length, &length, log) ; 
	cout << "Compile Error, Log Below\n" << log << "\n" ; 
	delete [] log ; 
}

void GpuProgram::setIsLight(bool isLight){
	glUniform1i(mIsLight, isLight);
}

void GpuProgram::setIsShaded(bool isShaded){
	glUniform1i(mIsShaded, isShaded);
}

void GpuProgram::setIsTex(bool isTex){
	glUniform1i(mIsTex, isTex);
}

void GpuProgram::setLights(vector<Light*> lights, mat4 modelViewMatrix){
	for (int i = 0; i < lights.size(); i++) {
		glUniform4fv(mLightPositions[i], 1, glm::value_ptr(modelViewMatrix * lights[i]->getPosition()));
		glUniform4fv(mLightColor[i], 1, glm::value_ptr(lights[i]->getColor()));
	}
}

void GpuProgram::setAmbient(vec4 ambient){
	glUniform4fv(mAmbient, 1, glm::value_ptr(ambient)); 
}

void GpuProgram::setDiffuse(vec4 diffuse){
	glUniform4fv(mDiffuse, 1, glm::value_ptr(diffuse));
}

void GpuProgram::setSpecular(vec4 specular){
	glUniform4fv(mSpecular, 1, glm::value_ptr(specular)); 
}

void GpuProgram::setEmission(vec4 emission){
	glUniform4fv(mEmission, 1, glm::value_ptr(emission)); 
}

void GpuProgram::setShininess(float shininess){
	glUniform1fv(mShininess, 1, &shininess); 
}

void GpuProgram::setLightNum(int lightNum){
	glUniform1i(mLightNum, lightNum);
}

void GpuProgram::setTex(int tex){
	glUniform1i(mTex, tex);
}

void GpuProgram::setDisplacementMap(int map) {
	glUniform1i(mDisplacementMap, map);
}

void GpuProgram::setIsDisplacementMap(bool isDisplacementMap){
	glUniform1i(mIsDisplacementMap, isDisplacementMap);
}