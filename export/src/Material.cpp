#include "Material.h"

using namespace std;

Material* Material::DEFAULT = new Material(
	"default",
	vec4(0.2, 0.2, 0.2, 1),
	vec4(0.8, 0.8, 0.8, 1),
	vec4(1.0, 1.0, 1.0, 1),
	vec4(0.0, 0.0, 0.0, 1),
	65,
	NULL
);

Material::Material(string name) 
	: mName(name)
{
}

Material::Material(string name, vec4 ambient, vec4 diffuse, vec4 specular, vec4 emission, float shininess, Texture* texture)
	: mName(name), mAmbient(ambient), mDiffuse(diffuse), mSpecular(specular), mEmission(emission), mShininess(shininess), mTexture(texture)
{
}


Material::~Material(void)
{
}