// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include <GL/glew.h>
#include <GL/glut.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <string>
#include <vector>

typedef glm::mat3 mat3;
typedef glm::mat4 mat4; 
typedef glm::vec2 vec2; 
typedef glm::vec3 vec3; 
typedef glm::vec4 vec4; 

using namespace std;