#include "ModelEntity.h"
#include "SceneManager.h"
#include "AssetManager.h"

ModelEntity::ModelEntity(void)
{
}

ModelEntity::~ModelEntity(void) {
	if (mModel) {
		delete mModel;
	}
}

ModelEntity::ModelEntity(Model* model) {
	mModel = model;
	mModel->scaleToUnit();
}

ModelEntity::ModelEntity(Model* model, mat4 transformMatrix) {
	mModel = model;
	mModel->scaleToUnit();
	mTransformMatrix = transformMatrix;
	mAnimation = NULL;
}

void ModelEntity::render(mat4 modelViewMatrix) {
	Entity::render(modelViewMatrix);
	mModel->render();
}

void ModelEntity::update() {
}

Model* ModelEntity::getModel() {
	return mModel;
}