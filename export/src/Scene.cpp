#include "Scene.h"

#include "Transform.h"
#include "FileTokenizer.h"
#include "ModelEntity.h"
#include "Material.h"
#include "FPSCamera.h"
#include "Teapot.h"
#include "Sphere.h"
#include "Cube.h"
#include "Plane.h"
#include "AnimationTrack.h"

#include <iostream>

using namespace std;

Scene::Scene(){
	mAssetManager = new AssetManager();
}

Scene::~Scene(void) {
	if (mCamera) {
		delete mCamera;
	}
	if (mAssetManager) {
		delete mAssetManager;
	}

	for (int i = 0; i < mLights.size(); i++) {
		if (mLights[i]) {
			delete mLights[i];
		}
	}
	mLights.clear();

	for (int i = 0; i < mEntities.size(); i++) {
		if (mEntities[i]) {
			delete mEntities[i];
		}
	}
	mEntities.clear();	
}

vector<Light*> Scene::getLights() const {
	return mLights;
}

vector<Entity*> Scene::getEntities() const {
	return mEntities;
}

Camera* Scene::getCamera() const {
	return mCamera;
}

mat4 Scene::getTransformMat() const {
	return mTransformMatrix;
}

int Scene::getWidth() const {
	return mWidth;
}

int Scene::getHeight() const {
	return mHeight;
}

void Scene::setSize(int width, int height) {
	mHeight = height;
	mWidth = width;
}

void Scene::loadScene(string file){
	FileTokenizer* tokenizer = new FileTokenizer(file.c_str());
	vector<vector<string>> lines = tokenizer->readAll();
	vector<vector<string>>::iterator it;
	
	if (lines.begin() == lines.end()) {
		return;
	}

	AnimationTrack* currentAnimation = NULL;
	mTransformMatrix = mat4(1.0);

	for (it = lines.begin() ; it < lines.end(); it++) {
		vector<string> command = *it;
		if (command[0].compare("size") == 0 && command.size() == 3) {
			this->mWidth = atoi(command[1].c_str());
			this->mHeight = atoi(command[2].c_str());

		} else if (command[0].compare("camera") == 0 && command.size() == 11) {
			mCamera = new Camera(vec3(atof(command[1].c_str()), atof(command[2].c_str()), atof(command[3].c_str())),
				vec3(atof(command[4].c_str()), atof(command[5].c_str()), atof(command[6].c_str())),
				vec3(atof(command[7].c_str()), atof(command[8].c_str()), atof(command[9].c_str())),
				atof(command[10].c_str()));

		} else if (command[0].compare("FPSCamera") == 0 && command.size() == 8) {
			mCamera = new FPSCamera(vec3(atof(command[1].c_str()), atof(command[2].c_str()), atof(command[3].c_str())),
				vec3(atof(command[4].c_str()), atof(command[5].c_str()), atof(command[6].c_str())),
				atof(command[7].c_str()));

		} else if (command[0].compare("light") == 0 && command.size() == 9) {
			mLights.push_back(new Light(atof(command[1].c_str()), atof(command[2].c_str()), atof(command[3].c_str()), atof(command[4].c_str()),
				atof(command[5].c_str()), atof(command[6].c_str()), atof(command[7].c_str()), atof(command[8].c_str())));

		} else if (command[0].compare("ambient") == 0 && command.size() == 5) {
			mAmbient = vec4(atof(command[1].c_str()), atof(command[2].c_str()), atof(command[3].c_str()), atof(command[4].c_str()));

		} else if (command[0].compare("diffuse") == 0 && command.size() == 5) {
			mDiffuse = vec4(atof(command[1].c_str()), atof(command[2].c_str()), atof(command[3].c_str()), atof(command[4].c_str()));

		} else if (command[0].compare("specular") == 0 && command.size() == 5) {
			mSpecular = vec4(atof(command[1].c_str()), atof(command[2].c_str()), atof(command[3].c_str()), atof(command[4].c_str()));

		} else if (command[0].compare("emission") == 0 && command.size() == 5) {
			mEmission = vec4(atof(command[1].c_str()), atof(command[2].c_str()), atof(command[3].c_str()), atof(command[4].c_str()));

		} else if (command[0].compare("shininess") == 0 && command.size() == 2) {
			mShininess = atof(command[1].c_str());

		} else if (command[0].compare("teapot") == 0 && command.size() == 2) {
			mEntities.push_back(new Teapot(atof(command[1].c_str()), mTransformMatrix, mAmbient, mDiffuse, mSpecular, mEmission, mShininess));

		} else if (command[0].compare("sphere") == 0 && command.size() == 2) {
			mEntities.push_back(new Sphere(atof(command[1].c_str()), mTransformMatrix, mAmbient, mDiffuse, mSpecular, mEmission, mShininess));

		} else if (command[0].compare("cube") == 0 && command.size() == 2) {
			mEntities.push_back(new Cube(atof(command[1].c_str()), mTransformMatrix, mAmbient, mDiffuse, mSpecular, mEmission, mShininess));

		} else if (command[0].compare("plane") == 0 && command.size() == 2) {
			mEntities.push_back(new Plane(atof(command[1].c_str()), mTransformMatrix, mAmbient, mDiffuse, mSpecular, mEmission, mShininess));

		} else if (command[0].compare("translate") == 0 && command.size() == 4) {
			mTransformMatrix = mTransformMatrix * glm::transpose(Transform::translate(atof(command[1].c_str()), atof(command[2].c_str()), atof(command[3].c_str())));

		} else if (command[0].compare("rotate") == 0 && command.size() == 5) {
			mTransformMatrix = mTransformMatrix * glm::transpose(Transform::rotate(vec3(atof(command[1].c_str()), atof(command[2].c_str()), atof(command[3].c_str())), glm::radians(atof(command[4].c_str()))));

		} else if (command[0].compare("scale") == 0 && command.size() == 4) {
			mTransformMatrix = mTransformMatrix * glm::transpose(Transform::scale(atof(command[1].c_str()), atof(command[2].c_str()), atof(command[3].c_str())));

		} else if (command[0].compare("pushTransform") == 0 && command.size() == 1) {
			mTransformMatrices.push(mTransformMatrix);

		} else if (command[0].compare("popTransform") == 0 && command.size() == 1) {
			mTransformMatrix =mTransformMatrices.top();
			mTransformMatrices.pop();

		} else if (command[0].compare("glmModel") == 0 && command.size() == 3) {
			GlmModel* model = mAssetManager->loadGlmModel(command[1], command[2]);

		} else if (command[0].compare("glmModel") == 0 && command.size() == 4) {
			GlmModel* model = mAssetManager->loadGlmModel(command[1], command[2], command[3]);

		} else if (command[0].compare("glmModel") == 0 && command.size() == 5 && command[4].compare("repeat") == 0) {
			GlmModel* model = mAssetManager->loadGlmModel(command[1], command[2], command[3], true);

		} else if (command[0].compare("glmModel") == 0 && command.size() == 5) {
			GlmModel* model = mAssetManager->loadGlmModel(command[1], command[2], command[3], command[4]);

		} else if (command[0].compare("rawModel") == 0 && command.size() == 3) {
			RawModel* model = mAssetManager->loadRawModel(command[1], command[2]);

		} else if (command[0].compare("objModel") == 0 && command.size() == 3) {
			ObjModel* model = mAssetManager->loadObjModel(command[1], command[2]);
			map<string, Mesh*>::const_iterator it;
			for(it = model->mMeshes.begin(); it != model->mMeshes.end(); ++it) {
				Mesh* mesh = (*it).second;
				mesh->generateVertexData(model->mVertices, model->mNormals, model->mTexCoords, model->mIndices);
			}
		} else if (command[0].compare("object") == 0 && command.size() == 2){
			Model* model = mAssetManager->getModel(command[1]);
			model->mMaterial = new Material(command[1], mAmbient, mDiffuse, mSpecular, mEmission, mShininess, NULL);
			Entity* entity = new ModelEntity(model, mTransformMatrix);
			if (currentAnimation) {
				entity->attachAnimation(currentAnimation);
				currentAnimation = NULL;
			}
			mEntities.push_back(entity);

		} else if (command[0].compare("animationRotate") == 0 && command.size() == 2) {
			currentAnimation = new AnimationTrack(atof(command[1].c_str()));

		} else {
			cerr << "invalid command in scene file: ";
			throw 6;
		}
	}

	createStarByHand();
	assert(mTransformMatrices.size() == 0);
}

void Scene::createStarByHand() {
	mTransformMatrix = mTransformMatrix * glm::transpose(Transform::translate(1, 6, -2));
	mTransformMatrix = mTransformMatrix * glm::transpose(Transform::scale(0.2, 0.2, 0.2));

	RawModel* model = new RawModel();
	Entity* entity = new ModelEntity(model, mTransformMatrix);
	mEntities.push_back(entity);

	model->mMaterial = new Material("starByHand", vec4(0.3, 0.3, 0.3, 1), vec4(0.3, 0.2, 0.1, 0), vec4(0.1, 0.1, 0.1, 1), vec4(0), 100, NULL);
	
	model->mVertices.push_back(vec3(0,5,0));
	model->mVertices.push_back(vec3(0,-0.661,1.5));
	model->mVertices.push_back(vec3(-1.17,1.1,0));
	model->mVertices.push_back(vec3(-1.17,1.1,0));
	model->mVertices.push_back(vec3(0,-0.661,1.5));
	model->mVertices.push_back(vec3(-5,1.1,0));
	model->mVertices.push_back(vec3(-5,1.1,0));
	model->mVertices.push_back(vec3(0,-0.661,1.5));
	model->mVertices.push_back(vec3(-1.9,-1.33,0));
	model->mVertices.push_back(vec3(-1.9,-1.33,0));
	model->mVertices.push_back(vec3(0,-0.661,1.5));
	model->mVertices.push_back(vec3(-3,-5,0));
	model->mVertices.push_back(vec3(-3,-5,0));
	model->mVertices.push_back(vec3(0,-0.661,1.5));
	model->mVertices.push_back(vec3(0,-1.7,0));
	model->mVertices.push_back(vec3(0,-1.7,0));
	model->mVertices.push_back(vec3(0,-0.661,1.5));
	model->mVertices.push_back(vec3(3,-5,0));
	model->mVertices.push_back(vec3(3,-5,0));
	model->mVertices.push_back(vec3(0,-0.661,1.5));
	model->mVertices.push_back(vec3(1.9,-1.33,0));
	model->mVertices.push_back(vec3(1.9,-1.33,0));
	model->mVertices.push_back(vec3(0,-0.661,1.5));
	model->mVertices.push_back(vec3(5,1.1,0));
	model->mVertices.push_back(vec3(5,1.1,0));
	model->mVertices.push_back(vec3(0,-0.661,1.5));
	model->mVertices.push_back(vec3(1.17,1.1,0));
	model->mVertices.push_back(vec3(1.17,1.1,0));
	model->mVertices.push_back(vec3(0,-0.661,1.5));
	model->mVertices.push_back(vec3(0,5,0));
	model->mVertices.push_back(vec3(1.17,1.1,0));
	model->mVertices.push_back(vec3(0,5,0));
	model->mVertices.push_back(vec3(0,-0.661,-1.5));
	model->mVertices.push_back(vec3(5,1.1,0));
	model->mVertices.push_back(vec3(1.17,1.1,0));
	model->mVertices.push_back(vec3(0,-0.661,-1.5));
	model->mVertices.push_back(vec3(1.9,-1.33,0));
	model->mVertices.push_back(vec3(5,1.1,0));
	model->mVertices.push_back(vec3(0,-0.661,-1.5));
	model->mVertices.push_back(vec3(3,-5,0));
	model->mVertices.push_back(vec3(1.9,-1.33,0));
	model->mVertices.push_back(vec3(0,-0.661,-1.5));
	model->mVertices.push_back(vec3(0,-1.7,0));
	model->mVertices.push_back(vec3(3,-5,0));
	model->mVertices.push_back(vec3(0,-0.661,-1.5));
	model->mVertices.push_back(vec3(-3,-5,0));
	model->mVertices.push_back(vec3(0,-1.7,0));
	model->mVertices.push_back(vec3(0,-0.661,-1.5));
	model->mVertices.push_back(vec3(-1.9,-1.33,0));
	model->mVertices.push_back(vec3(-3,-5,0));
	model->mVertices.push_back(vec3(0,-0.661,-1.5));
	model->mVertices.push_back(vec3(-5,1.1,0));
	model->mVertices.push_back(vec3(-1.9,-1.33,0));
	model->mVertices.push_back(vec3(0,-0.661,-1.5));
	model->mVertices.push_back(vec3(-1.17,1.1,0));
	model->mVertices.push_back(vec3(-5,1.1,0));
	model->mVertices.push_back(vec3(0,-0.661,-1.5));
	model->mVertices.push_back(vec3(0,5,0));
	model->mVertices.push_back(vec3(-1.17,1.1,0));
	model->mVertices.push_back(vec3(0,-0.661,-1.5));
}

void Scene::render(mat4 modelViewMatrix) {
	for (int i = 0; i < mEntities.size(); i++) {
		mEntities[i]->render(modelViewMatrix);
	}
}

AssetManager* Scene::getAssetManager() const{
	return mAssetManager;
}