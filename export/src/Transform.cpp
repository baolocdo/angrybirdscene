#include "Transform.h"

void Transform::left(float degrees, vec3& eye, vec3& up)
{
	vec3 w = eye; 
	vec3 u = glm::cross(up, w);
	vec3 v = glm::cross(w, u);
	v = glm::normalize(v);
	mat3 M = rotate(glm::radians(degrees), v);
	eye = eye * M;
}

void Transform::up(float degrees, vec3& eye, vec3& up)
{
	vec3 w = eye;
	vec3 u = glm::cross(up, w);
	u = glm::normalize(u);
	mat3 M = rotate(-glm::radians(degrees), u);
	eye = eye * M;
	up = up * M;
}

mat4 Transform::lookAt(const vec3& eye, const vec3& center, const vec3& up)
{
	mat4 M ; 
	vec3 a = eye - center;
	vec3 w = glm::normalize(a);

	vec3 u = glm::cross(up, w);
	u = glm::normalize(u);
	vec3 v = glm::cross(w, u);
	v = glm::normalize(v);

	M[0] = vec4(u.x, u.y, u.z, -eye.x*u.x - eye.y*u.y - eye.z*u.z);
	M[1] = vec4(v.x, v.y, v.z, -eye.x*v.x - eye.y*v.y - eye.z*v.z);
	M[2] = vec4(w.x, w.y, w.z, -eye.x*w.x - eye.y*w.y - eye.z*w.z);
	M[3] = vec4(0, 0, 0, 1);
	return M ; 
}

mat4 Transform::perspective(float fovy, float aspect, float zNear, float zFar)
{
	float a = -(zFar + zNear) / (zFar - zNear);
	float b = - (2 * zFar * zNear) / (zFar - zNear);
	float d = 1 / tan(glm::radians(fovy) / 2);

	mat4 M; 

	M[0] = vec4(d/aspect, 0, 0, 0);
	M[1] = vec4(0, d, 0, 0);
	M[2] = vec4(0, 0, a, b);
	M[3] = vec4(0, 0, -1, 0);

	return M;
}

mat3 Transform::rotate(const float degrees, const vec3& axis)
{
	mat3 R ; 
	// FILL IN YOUR CODE HERE
	mat3 identity = mat3(1.0), aa, A;

	//find x, y, z of the axis
	float x = axis[0];
	float y = axis[1];
	float z = axis[2];

	//find aaT
	aa[0] = vec3(x*x, x*y, x*z);
	aa[1] = vec3(x*y, y*y, y*z);
	aa[2] = vec3(x*z, y*z, z*z);

	//find A*
	A[0] = vec3(0, -z, y);
	A[1] = vec3(z, 0, -x);
	A[2] = vec3(-y, x, 0);

	R = identity * cos(degrees) + (1 - cos(degrees)) * aa + sin(degrees) * A;
	return R ; 
}

mat4 Transform::rotate(vec4 quaternion)
{
	quaternion = glm::normalize(quaternion);

	float x2 = quaternion.x * quaternion.x;
	float y2 = quaternion.y * quaternion.y;
	float z2 = quaternion.z * quaternion.z;
	float xy = quaternion.x * quaternion.y;
	float xz = quaternion.x * quaternion.z;
	float yz = quaternion.y * quaternion.z;
	float wx = quaternion.w * quaternion.x;
	float wy = quaternion.w * quaternion.y;
	float wz = quaternion.w * quaternion.z;

	mat4 R ; 
	R[0] = vec4(1.0f - 2.0f * (y2 + z2), 2.0f * (xy - wz), 2.0f * (xz + wy), 0.0f);
	R[1] = vec4(2.0f * (xy + wz), 1.0f - 2.0f * (x2 + z2), 2.0f * (yz - wx), 0.0f);
	R[2] = vec4(2.0f * (xz - wy), 2.0f * (yz + wx), 1.0f - 2.0f * (x2 + y2), 0.0f);
	R[3] = vec4(0.0f, 0.0f, 0.0f, 1.0f);
	return R ; 
}

mat4 Transform::rotate(vec3 axis, float angle) {
	float c = glm::cos(-angle);
	float s = glm::sin(-angle);
    float t = 1.0f - c;

	vec3 NormalizedAxis = glm::normalize(axis);
    float x = NormalizedAxis.x;
    float y = NormalizedAxis.y;
    float z = NormalizedAxis.z;

    mat4 Result;
    Result[0] = vec4(1 + t*(x*x-1), z*s+t*x*y, -y*s+t*x*z, 0.0f);
    Result[1] = vec4 (-z*s+t*x*y, 1+t*(y*y-1), x*s+t*y*z, 0.0f);
    Result[2] = vec4(y*s+t*x*z, -x*s+t*y*z, 1+t*(z*z-1), 0.0f);
	Result[3] = vec4(0.0f, 0.0f, 0.0f, 1.0f);
    return Result;
}
mat4 Transform::scale(const float &sx, const float &sy, const float &sz) 
{
	mat4 scale;
	scale[0] = vec4(sx, 0, 0, 0);
	scale[1] = vec4(0, sy, 0, 0);
	scale[2] = vec4(0, 0, sz, 0);
	scale[3] = vec4(0, 0, 0, 1);

	return scale;
}

mat4 Transform::translate(const float &tx, const float &ty, const float &tz)
{
	mat4 translate;
	translate[0] = vec4(1, 0, 0, tx);
	translate[1] = vec4(0, 1, 0, ty);
	translate[2] = vec4(0, 0, 1, tz);
	translate[3] = vec4(0, 0, 0, 1);

	return translate;
}

Transform::Transform()
{

}

Transform::~Transform()
{

}
