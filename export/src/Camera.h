#pragma once

#include "stdafx.h"

class Camera
{
public:
	Camera();
	Camera(vec3 position, vec3 up, vec3 lookAt, float fov);
	Camera(vec3 position, vec3 up, vec3 lookAt, float fov, float zMin, float zMax);
	~Camera(void);

	// TODO: refactor these properties into camera.cpp
	// bad style here
	vec3 getPosition() const {
		return mPosition;
	}	
	vec3 getUp() const {
		return mUp;
	}
	vec3 getLookAt() const {
		return mLookAt;
	}

	float getFov() const {
		return mFov;
	}
	void setFov(float fov) {
		mFov = fov;
	}

	float getZMin() const {
		return mZMin;
	}
	void setZMin(float zMin) {
		mZMin = zMin;
	}

	float getZMax() const {
		return mZMax;
	}
	void setZMax(float zMax) {
		mZMax = zMax;
	}

	virtual void moveForward(float amount);
	virtual void moveBackward(float amount);
	virtual void moveLeft(float amount);
	virtual void moveRight(float amount);

	virtual void yaw(float degree);
	virtual void pitch(float degree);
	virtual void roll(float degree);

	mat4 getModelViewMatrix();

protected:
	vec3 mPosition;
	vec3 mLookAt;
	vec3 mUp;
	vec3 mLeft;
	
	vec3 mInitialPosition;
	vec3 mInitialUp;
	vec3 mInitialLookAt;

	float mFov;
	float mZMin;
	float mZMax;
};

