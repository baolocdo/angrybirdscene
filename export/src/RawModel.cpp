#include "RawModel.h"
#include "SceneManager.h"

RawModel::RawModel(void)
{
}


RawModel::~RawModel(void)
{
	if (mMaterial) {
		delete mMaterial;
	}
}

void RawModel::scaleToUnit() {
	//glmUnitize(mGLMModel);
}

void RawModel::render() {
	//glCullFace(GL_FRONT_AND_BACK);
  
	SceneManager::getInstance()->getGpuProgram()->setIsTex(false);
	SceneManager::getInstance()->getGpuProgram()->setAmbient(mMaterial->mAmbient);
	SceneManager::getInstance()->getGpuProgram()->setDiffuse(mMaterial->mDiffuse);
	SceneManager::getInstance()->getGpuProgram()->setSpecular(mMaterial->mSpecular);
	SceneManager::getInstance()->getGpuProgram()->setEmission(mMaterial->mEmission);
	SceneManager::getInstance()->getGpuProgram()->setShininess(mMaterial->mShininess);

	glBegin(GL_TRIANGLES);
	for (int i = 0; i < mVertices.size(); i++) {
		glVertex3fv(glm::value_ptr(mVertices[i]));
	}
	glEnd();
}