#pragma once

#include "stdafx.h"
#include "Entity.h"

class Sphere :
	public Entity
{
public:
	Sphere(float size, mat4 tranformMatrix, vec4 ambient, vec4 diffuse, vec4 specular, vec4 emission, float shininess);
	~Sphere(void);
	virtual void render(mat4 modelViewMatrix);
};

