#pragma once
#include "Model.h"
#include "Material.h"

class RawModel :
	public Model
{
public:
	RawModel(void);
	~RawModel(void);

	virtual void scaleToUnit();
	virtual void render();

	vector<vec3> mVertices;
};

