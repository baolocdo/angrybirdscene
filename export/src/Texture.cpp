#include "Texture.h"


Texture::Texture(string file)
{
}

Texture::~Texture(void)
{
}

string Texture::getName() const{
	return mName;
}

void Texture::setName(string name){
	mName = name;
}

GLuint Texture::getID() {
	return mID;
}

void Texture::setID(GLuint id){
	mID = id;
}

GLfloat Texture::getWidth() const{
	return mWidth;
}

void Texture::setWidth(GLfloat width){
	mWidth = width;
}

GLfloat Texture::getHeight() const{
	return mHeight;
}

void Texture::setHeight(GLfloat height){
	mHeight = height;
}