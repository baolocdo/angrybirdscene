# version 120 

// Mine is an old machine.  For version 130 or higher, do 
// in vec4 color ;  
// in vec4 mynormal ; 
// in vec4 myvertex ;
// That is certainly more modern

varying vec4 color ;
varying vec3 mynormal ; 
varying vec4 myvertex ; 

//Texture mapping
uniform sampler2D tex ; 
uniform int isTex ; 

//Shadow Mapping
//uniform sampler2D ShadowMap;
//varying vec4 ShadowCoord;

const int LIGHT_NUM = 10; 

uniform int lightNum;
uniform int isLight ; // are we lighting. 
uniform int isShaded;

// Assume light 0 and light 1 are both point lights
// The actual light values are passed from the main OpenGL program. 
// This could of course be fancier.  My goal is to illustrate a simple idea. 

uniform vec4 lightposn[LIGHT_NUM];
uniform vec4 lightcolor[LIGHT_NUM]; 

// Now, set the material parameters.  These could be varying and/or bound to 
// a buffer.  But for now, I'll just make them uniform.  
// I use ambient, diffuse, specular, shininess as in OpenGL.  
// But, the ambient is just additive and doesn't multiply the lights.  

uniform vec4 ambient ; 
uniform vec4 diffuse ; 
uniform vec4 specular ; 
uniform vec4 emission;
uniform float shininess ; 

vec4 ComputeLight (const in vec3 direction, const in vec4 lightcolor, const in vec3 normal, const in vec3 halfvec, const in vec4 mydiffuse, const in vec4 myspecular, const in float myshininess) {

        float nDotL = dot(normal, direction)  ;         
        vec4 lambert = mydiffuse * lightcolor * max (nDotL, 0.0) ;  

        float nDotH = dot(normal, halfvec) ; 
        vec4 phong = myspecular * lightcolor * pow (max(nDotH, 0.0), myshininess) ; 

        vec4 retval = lambert + phong ; 
        return retval ;            
}       

void main (void) 
{       
	if (isShaded == 0) 
		gl_FragColor = color ; 
    else { 
        // They eye is always at (0,0,0) looking down -z axis 
        // Also compute current fragment position and direction to eye 

        const vec3 eyepos = vec3(0,0,0) ; 
        vec4 _mypos = gl_ModelViewMatrix * myvertex ; 
        vec3 mypos = _mypos.xyz / _mypos.w ; // Dehomogenize current location 
        vec3 eyedirn = normalize(eyepos - mypos) ; 

        // Compute normal, needed for shading. 
        // Simpler is vec3 normal = normalize(gl_NormalMatrix * mynormal) ; 
        vec3 _normal = (gl_ModelViewMatrixInverseTranspose*vec4(mynormal,0.0)).xyz ; 
        vec3 normal = normalize(_normal) ;

		if (isTex > 0) 
			gl_FragColor = texture2D(tex, gl_TexCoord[0].st) ; 
		else
			gl_FragColor = ambient + emission;

		vec3 direction;
		vec3 position;

		if (isLight != 0)
		{
			for (int i = 0; i < lightNum; i++)
			{
				if (lightposn[i].w != 0)
				{
					position = lightposn[i].xyz / lightposn[i].w ; 
					direction = normalize (position - mypos) ; // no attenuation
				}
				else
					direction = normalize(vec3(lightposn[i].xyz));
			 
				vec3 halfd = normalize (direction + eyedirn) ;  
				vec4 col = ComputeLight(direction, lightcolor[i], normal, halfd, diffuse, specular, shininess) ;

				//Update gl_FragColor
				gl_FragColor += col;
			}         
		}
	}
}
