#include "FileTokenizer.h"
#include <istream>
#include <iterator>
#include <sstream>

FileTokenizer::FileTokenizer(string file)
	: mFile(file)
{
}

vector<vector<string>> FileTokenizer::readAll() {
	ifstream input;
	string line;
	vector<vector<string>> ret;

	string path;	
	path.assign(mFile);
	path = "./data/" + path;
	input.open(path);
	if (input.is_open()) {
		getline(input, line);
		while (input) {
			line.erase(0, line.find_first_not_of(' '));
			int firstCharPosition = line.find_first_not_of(" \t\r\n");
			if (firstCharPosition != string::npos && line[firstCharPosition] != '#') {
				ret.push_back(tokenize(line));
			}
			getline(input, line);
		}
	}

	return ret;
}


vector<string> FileTokenizer::tokenize(string str) {
	vector<string> tokens;
	istringstream iss(str);
	copy(istream_iterator<string>(iss),
		istream_iterator<string>(),
		back_inserter<vector<string> >(tokens));
	return tokens;
}
