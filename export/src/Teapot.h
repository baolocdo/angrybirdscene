#pragma once

#include "Entity.h"
#include "stdafx.h"

class Teapot :
	public Entity
{
public:
	Teapot(float size, mat4 tranformMatrix, vec4 ambient, vec4 diffuse, vec4 specular, vec4 emission, float shininess);
	~Teapot(void);
	virtual void render(mat4 modelViewMatrix);
};

