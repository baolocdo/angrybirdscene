#pragma once

#include "stdafx.h"
#include <map>

class InputManager
{
	enum KeyState {
		KS_DOWN,
		KS_UP,
		KS_PRESSED,
	};

public:
	InputManager(void);
	~InputManager(void);
	void onKeyDown(int key, int x, int y);
	void onKeyUp(int key, int x, int y);
	bool isKeyDown(int key);
	bool isKeyPressed(int key);
	bool consumeKeyPress(int key);

private:
	map<int, KeyState> mKeyboard;
};

