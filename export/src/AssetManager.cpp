#include "AssetManager.h"
#include "FileTokenizer.h"
#include "TGALoader.h"
#include "Triangle.h"
#include "Mesh.h"
#include "RawModel.h"

#include <iostream>

AssetManager::AssetManager(void)
{
}


AssetManager::~AssetManager(void)
{
	map<string, Model*>::const_iterator itModel;
	for(itModel = mModels.begin(); itModel != mModels.end(); ++itModel) {
		Model* model = (*itModel).second;
		if (model) {
			delete model;
		}
	}
	map<string, Texture*>::const_iterator itTexture;
	for(itTexture = mTextures.begin(); itTexture != mTextures.end(); ++itTexture) {
		Texture* texture = (*itTexture).second;
		if (texture) {
			delete texture;
		}
	}
	map<string, Material*>::const_iterator itMaterial;
	for(itMaterial = mMaterials.begin(); itMaterial != mMaterials.end(); ++itMaterial) {
		Material* material = (*itMaterial).second;
		if (material) {
			delete material;
		}
	}

}

Model* AssetManager::getModel(string name) {
	return mModels.find(name)->second;
}

ObjModel* AssetManager::loadObjModel(string name, string file) {
	// having space inside means the file cannot have a material name like this, so it's safe.
	//model->mMaterials[" DEFAULT "] = currentMaterial;

	FileTokenizer* tokenizer = new FileTokenizer(file.c_str());
	vector<vector<string>> lines = tokenizer->readAll();
	vector<vector<string>>::iterator it;

	if (lines.begin() == lines.end()) {
		return NULL;
	}

	// the return model
	ObjModel* model = new ObjModel();
	Mesh* currentMesh = NULL;

	model->mPathName = file.c_str();
	model->mPosition = vec3(0,0,0);

	for (it = lines.begin() ; it < lines.end(); it++) {
		vector<string> command = *it;
		if (command[0].compare("g") == 0 && command.size() == 2) {
		} else if (command[0].compare("s") == 0) {
			// TODO: implement smooth
		} else if (command[0].compare("o") == 0 && command.size() == 2) {
			// TODO: implement object
		} else if (command[0].compare("mtllib") == 0 && command.size() == 2) {
			loadMaterialLib(command[1]);
		} else if (command[0].compare("usemtl") == 0 && command.size() == 2) {
			currentMesh = model->mMeshes[command[1]] = new Mesh(command[1]);
			if (mMaterials.find(command[1])->second) {
				currentMesh->mMaterial = mMaterials.find(command[1])->second;
			} else {
				currentMesh->mMaterial = Material::DEFAULT;
			}
		} else if (command[0].compare("v") == 0 && command.size() == 4) {
			model->mVertices.push_back(vec3(atof(command[1].c_str()),atof(command[2].c_str()),atof(command[3].c_str())));
		} else if (command[0].compare("vt") == 0 && command.size() == 3) {
			model->mTexCoords.push_back(vec3(atof(command[1].c_str()),atof(command[2].c_str()), 0.5));
		} else if (command[0].compare("vt") == 0 && command.size() == 4) {
			model->mTexCoords.push_back(vec3(atof(command[1].c_str()),atof(command[2].c_str()), atof(command[3].c_str())));
		} else if (command[0].compare("vn") == 0 && command.size() == 4) {
			model->mNormals.push_back(vec3(atof(command[1].c_str()),atof(command[2].c_str()),atof(command[3].c_str())));
		} else if (command[0].compare("f") == 0 && command.size() == 4) {
			Triangle* triangle = new Triangle();
			int v, t, n;
			for (int i = 0; i < 3; i++) {
				sscanf(command[i + 1].c_str(), "%d/%d/%d", &v, &t, &n);
				triangle->getVIndices()[i] = v - 1;
				triangle->getTIndices()[i] = t - 1;
				triangle->getNIndices()[i] = n - 1;
				model->mIndices.push_back(v - 1);
			}
			currentMesh->mTriangles.push_back(triangle);
		} else {
			cerr << "invalid command in mat file: ";
			throw 6;
		}
	}
	model->scaleToUnit();

	mModels[name] = model;
	return model;
}

RawModel* AssetManager::loadRawModel(string name, string file) {
	FileTokenizer* tokenizer = new FileTokenizer(file.c_str());
	vector<vector<string>> lines = tokenizer->readAll();
	vector<vector<string>>::iterator it;

	if (lines.begin() == lines.end()) {
		return NULL;
	}

	RawModel* model = new RawModel();
	for (it = lines.begin() ; it < lines.end(); it++) {
		vector<string> command = *it;
		if (command.size() == 9) {
			model->mVertices.push_back(vec3(atof(command[0].c_str()),atof(command[1].c_str()),atof(command[2].c_str())));
			model->mVertices.push_back(vec3(atof(command[3].c_str()),atof(command[4].c_str()),atof(command[5].c_str())));
			model->mVertices.push_back(vec3(atof(command[6].c_str()),atof(command[7].c_str()),atof(command[8].c_str())));
		} else {
			cerr << "invalid command in mat file: ";
			throw 6;
		}
	}

	mModels[name] = model;
	return model;
}

GlmModel* AssetManager::loadGlmModel(string name, string file) {
	GlmModel* model = new GlmModel();
	file = "./data/" + file;
	model->mGLMModel = glmReadOBJ((char*)file.c_str());
	model->scaleToUnit();
	model->mIsTextured = false;

	mModels[name] = model;
	return model;
}

GlmModel* AssetManager::loadGlmModel(string name, string file, string tgaFile) {
	GlmModel* model = new GlmModel();
	file = "./data/" + file;
	tgaFile = "./data/" + tgaFile;
	model->mGLMModel = glmReadOBJ((char*)file.c_str());
	model->scaleToUnit();
	model->mIsTextured = true;
	glActiveTexture(GL_TEXTURE0);
	model->mTextureID = loadTexture(tgaFile, false);

	mModels[name] = model;
	return model;
}

GlmModel* AssetManager::loadGlmModel(string name, string file, string tgaFile, bool repeat) {
	GlmModel* model = new GlmModel();
	file = "./data/" + file;
	tgaFile = "./data/" + tgaFile;
	model->mGLMModel = glmReadOBJ((char*)file.c_str());
	model->scaleToUnit();
	model->mIsTextured = true;
	glActiveTexture(GL_TEXTURE0);
	model->mTextureID = loadTexture(tgaFile, repeat);

	mModels[name] = model;
	return model;
}

GlmModel* AssetManager::loadGlmModel(string name, string file, string tgaFile, string mapFile) {
	GlmModel* model = new GlmModel();
	file = "./data/" + file;
	tgaFile = "./data/" + tgaFile;
	mapFile = "./data/" + mapFile;
	model->mGLMModel = glmReadOBJ((char*)file.c_str());
	model->scaleToUnit();
	model->mIsTextured = true;
	glActiveTexture(GL_TEXTURE0);
	model->mTextureID = loadTexture(tgaFile, false);
	model->mIsDisplacementMap = true;
	glActiveTexture(GL_TEXTURE1);
	model->mDisplacementMapID = loadTexture(mapFile, false);

	mModels[name] = model;
	return model;
}

void AssetManager::loadMaterialLib(string file) {
	// TODO: ensure that mtllib is in the same directory

	FileTokenizer* tokenizer = new FileTokenizer(file.c_str());
	vector<vector<string>> lines = tokenizer->readAll();
	vector<vector<string>>::iterator it;

	if (lines.begin() == lines.end()) {
		return;
	}

	Material* currentMaterial;// = Material(Material::DEFAULT);

	for (it = lines.begin() ; it < lines.end(); it++) {
		vector<string> command = *it;
		if (command[0].compare("newmtl") == 0 && command.size() == 2) {
			currentMaterial = mMaterials[command[1]] = new Material(command[1]);
			currentMaterial->mTexture = NULL;
		} else if (command[0].compare("Ns") == 0 && command.size() == 2) {
			currentMaterial->mShininess = atof(command[1].c_str()) * 128.0 / 1000.0;
		} else if (command[0].compare("Ka") == 0 && command.size() == 4) {
			currentMaterial->mAmbient = vec4(atof(command[1].c_str()), atof(command[2].c_str()), atof(command[3].c_str()), 1.0);
		} else if (command[0].compare("Ks") == 0 && command.size() == 4) {
			currentMaterial->mSpecular = vec4(atof(command[1].c_str()), atof(command[2].c_str()), atof(command[3].c_str()), 1.0);
		} else if (command[0].compare("Kd") == 0 && command.size() == 4) {
			currentMaterial->mDiffuse = vec4(atof(command[1].c_str()), atof(command[2].c_str()), atof(command[3].c_str()), 1.0);
		} else if (command[0].compare("d") == 0 && command.size() == 2) {
			//currentMaterial->mDiffuse = vec4(atof(command[1].c_str()), atof(command[2].c_str()), atof(command[3].c_str()), 1.0);
		} else if (command[0].compare("map_Kd") == 0 && command.size() == 2) {
			currentMaterial->mTexture = loadTexture(command[1].c_str());
		} else {
			cerr << "invalid command in mat file: ";
			exit(6);
		}
	}
}

Texture* AssetManager::loadTexture(string file) {
	float w, h;
	Texture* texture = NULL;
	if (mTextures.find(file) != mTextures.end()) {
		texture = mTextures.find(file)->second;
	}
	if (!texture) {
		texture = new Texture(file);
		mTextures[file] = texture;

		TGATexture tgaImg;
		LoadTGA(&tgaImg, (char*)file.c_str());

		if (!tgaImg.imageData) {
			cerr << "null tga texture, maybe wrong file name?";
		}
		texture->setWidth(tgaImg.width);
		texture->setHeight(tgaImg.height);

		GLuint id = texture->getID();

		glGenTextures(1, &id);
		texture->setID(id);

		glBindTexture(GL_TEXTURE_2D, id);
		glTexImage2D(GL_TEXTURE_2D, 0, tgaImg.type, tgaImg.width, tgaImg.height, 0, tgaImg.type, GL_UNSIGNED_BYTE, tgaImg.imageData);   
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		// TODO:
		free(tgaImg.imageData);
	}
	return texture;
}

GLuint AssetManager::loadTexture(string file, bool repeat) {
	TGATexture tgaImg;
	GLuint id = NULL;
	if (LoadTGA(&tgaImg, (char*)file.c_str())) {
		glGenTextures(1, &id);

		glBindTexture(GL_TEXTURE_2D, id);
		glTexImage2D(GL_TEXTURE_2D, 0, tgaImg.bpp / 8, tgaImg.width, tgaImg.height, 0, tgaImg.type, GL_UNSIGNED_BYTE, tgaImg.imageData);   
		
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		if (repeat) {
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		}

		if (tgaImg.imageData) {
			free(tgaImg.imageData);
		}
	}

	return id;
}