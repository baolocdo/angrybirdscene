#pragma once

#include "stdafx.h"
#include "Camera.h"
#include "Light.h"
#include "Entity.h"
#include "AssetManager.h"

#include <string>
#include <vector>
#include <stack>

using namespace std;

class Scene
{
public:
	Scene(void);
	~Scene(void);

	AssetManager* getAssetManager() const;
	
	void loadScene(string file);

	vector<Light*> getLights() const;
	vector<Entity*> getEntities() const;
	Camera* getCamera() const;

	mat4 getTransformMat() const;
	int getWidth() const;
	int getHeight() const;

	void setSize(int width, int height);
	void update(float timeElapsed);
	void render(mat4 modelViewMatrix);
	void init();

	void createStarByHand();

	// TODO: getter setter
private:
	AssetManager* mAssetManager;
	Camera* mCamera;
	vector<Light*> mLights;
	vector<Entity*> mEntities;

	mat4 mTransformMatrix;
	stack<mat4> mTransformMatrices;

	int mHeight;
	int mWidth;

	vec4 mAmbient;
	vec4 mDiffuse;
	vec4 mSpecular;
	vec4 mEmission;
	float mShininess;
};

