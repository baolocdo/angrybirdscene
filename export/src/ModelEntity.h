#pragma once

#include "stdafx.h"
#include "Entity.h"
#include "Model.h"

class ModelEntity :
	public Entity
{
public:
	ModelEntity(void);
	ModelEntity(Model* model);
	ModelEntity(Model* model, mat4 transformMatrix);
	~ModelEntity(void);

	virtual void render(mat4 modelViewMatrix);
	virtual void update();

	Model* getModel();
	
private:
	Model* mModel;
};

